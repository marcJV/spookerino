//
//  GlowableSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/18/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class GlowableSprite {
  fileprivate var foregroundSprite : SKSpriteNode!
  fileprivate var glowSprite : SKSpriteNode!
  open var isGlowing = false
  open var glowColor = SKColor.red
  fileprivate var glowSpeed : TimeInterval = 0.35
  open var timerDuration : TimeInterval = 2.0
  open var canInteract = true {
    didSet {
      if !canInteract {
        glowSprite.alpha = 0
      }
    }
  }

  open func setupSprite(_ imageName : String) {
    foregroundSprite = SKSpriteNode(imageNamed: imageName)

    glowSprite = SKSpriteNode(imageNamed: imageName + "-glow")
    glowSprite.xScale = 1.05
    glowSprite.yScale = 1.02

    glowSprite.color = glowColor
    glowSprite.colorBlendFactor = 1.0
    glowSprite.alpha = 0
  }

  open func getRootSprite () -> SKSpriteNode {
    return foregroundSprite
  }

  open func getChildren() -> [SKNode] {
    return [foregroundSprite, glowSprite]
  }

  open func startGlowing() {
    if canInteract {
      isGlowing = true
      let fadeIn = SKAction.fadeIn(withDuration: glowSpeed)

      glowSprite.run(fadeIn, withKey: "fade")
    }
  }

  open func stopGlowing() {
    isGlowing = false
    let fadeOut = SKAction.fadeOut(withDuration: glowSpeed)

    glowSprite.run(fadeOut, withKey: "fade")
  }

  open func containsPoint(_ p: CGPoint) -> Bool {
    if canInteract {
      return foregroundSprite.contains(p)
    }

    return false
  }

  open func intersectsNode(_ node: SKNode) -> Bool {
    return foregroundSprite.intersects(node)
  }

  open var zPosition: CGFloat = 1{
    didSet {
      foregroundSprite.zPosition = zPosition + 1
      glowSprite.zPosition = zPosition
    }
  }

  open var position: CGPoint = CGPoint() {
    didSet {
      foregroundSprite.position = position
      glowSprite.position = position
    }
  }
}
