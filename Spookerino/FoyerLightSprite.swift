//
//  FoyerLightSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/21/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class FoyerLightSprite : GlowableSprite, InteractiveSprite {
  fileprivate var lightGlow : SKSpriteNode!

  override open func setupSprite(_ imageName: String) {
    super.setupSprite(imageName)

    var lightPosition = position
    lightPosition.y = getRootSprite().size.height / 2 - 7

    lightGlow = SKSpriteNode(imageNamed: "light-on")
    lightGlow.position = lightPosition
    lightGlow.zPosition = zPosition + 1
    lightGlow.alpha = 0

    getRootSprite().addChild(lightGlow)
  }

  open func interact() {
    canInteract = false

    let lightOnAction = SKAction.fadeAlpha(to: 1.0, duration: 0.1)
    let lightOffAction = SKAction.fadeAlpha(to: 0.0, duration: 0.1)

    lightGlow.run(SKAction.repeatForever(SKAction.sequence([lightOnAction,
      SKAction.wait(forDuration: 0.2),
      lightOffAction,
      lightOnAction,
      lightOffAction,
      SKAction.wait(forDuration: 0.5)])))
  }
}
