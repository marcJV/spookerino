//
//  NPCZoneSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/18/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class NPCZoneSprite : SKSpriteNode {
  open var roomPaths = [CGPath]()
  open var exitPath : CGPath!
  open var startPoint = CGPoint()
  open var containsExit = false
  open var quadrant : NPCZone!
}

public enum NPCZone {
  case topLeftQuad
  case topRightQuad
  case bottomLeftQuad
  case bottomRightQuad
}
