//
//  HudSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/19/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class HudSprite : SKNode, Updateable {
  fileprivate let screenHeight : CGFloat = UIScreen.main.bounds.height
  fileprivate let screenWidth : CGFloat = UIScreen.main.bounds.width

  fileprivate var heart1 : SKSpriteNode!
  fileprivate var heart2 : SKSpriteNode!
  fileprivate var heart3 : SKSpriteNode!

  fileprivate var cross1 : SKSpriteNode!
  fileprivate var cross2 : SKSpriteNode!
  fileprivate var cross3 : SKSpriteNode!

  fileprivate var life = 3
  fileprivate var ghostHunters = 3

  open func setupHud() {
    zPosition = 1000
    let heartSide : CGFloat = 50
    let margin : CGFloat = 20
    let betweenHeartMargin : CGFloat = 10

    heart1 = SKSpriteNode(imageNamed: "heart")
    heart1.size = CGSize(width: 50, height: 50)
    heart1.position = CGPoint(x: margin + heartSide / 2, y: screenHeight - heartSide / 2 - margin)
    heart1.zPosition = 1000

    heart2 = SKSpriteNode(imageNamed: "heart")
    heart2.size = CGSize(width: 50, height: 50)
    heart2.position = CGPoint(x: margin + betweenHeartMargin + heartSide * 1.5, y: screenHeight - heartSide / 2 - margin)
    heart2.zPosition = 1000

    heart3 = SKSpriteNode(imageNamed: "heart")
    heart3.size = CGSize(width: 50, height: 50)
    heart3.position = CGPoint(x: margin + betweenHeartMargin * 2 + heartSide * 2.5,
                              y: screenHeight - heartSide / 2 - margin)
    heart3.zPosition = 1000

    let emptyHeart1 = SKSpriteNode(imageNamed: "emptyHeart")
    emptyHeart1.size = CGSize(width: 50, height: 50)
    emptyHeart1.position = heart1.position
    emptyHeart1.zPosition = 999

    let emptyHeart2 = SKSpriteNode(imageNamed: "emptyHeart")
    emptyHeart2.size = CGSize(width: 50, height: 50)
    emptyHeart2.position = heart2.position
    emptyHeart2.zPosition = 999

    let emptyHeart3 = SKSpriteNode(imageNamed: "emptyHeart")
    emptyHeart3.size = CGSize(width: 50, height: 50)
    emptyHeart3.position = heart3.position
    emptyHeart3.zPosition = 999

    addChild(heart1)
    addChild(heart2)
    addChild(heart3)

    addChild(emptyHeart1)
    addChild(emptyHeart2)
    addChild(emptyHeart3)

    let headSize = CGSize(width: 50, height: 42)

    let yellowHead = SKSpriteNode(imageNamed: "yellow-dude-head")
    yellowHead.size = headSize
    yellowHead.zPosition = 999
    yellowHead.position = CGPoint(x: screenWidth - margin - headSize.width, y: screenHeight - headSize.height - margin)

    let purpleHead = SKSpriteNode(imageNamed: "purple-dude-head")
    purpleHead.size = headSize
    purpleHead.zPosition = 999
    purpleHead.position = CGPoint(x: yellowHead.position.x - margin - headSize.width, y: screenHeight - headSize.height - margin)

    let greenHead = SKSpriteNode(imageNamed: "green-dude-head")
    greenHead.size = headSize
    greenHead.zPosition = 999
    greenHead.position = CGPoint(x: purpleHead.position.x - margin - headSize.width, y: screenHeight - headSize.height - margin)

    addChild(yellowHead)
    addChild(purpleHead)
    addChild(greenHead)

    cross1 = SKSpriteNode(imageNamed: "x")
    cross1.size = headSize
    cross1.position = yellowHead.position
    cross1.zPosition = 1000
    cross1.alpha = 0

    cross2 = SKSpriteNode(imageNamed: "x")
    cross2.size = headSize
    cross2.position = purpleHead.position
    cross2.zPosition = 1000
    cross2.alpha = 0

    cross3 = SKSpriteNode(imageNamed: "x")
    cross3.size = headSize
    cross3.position = greenHead.position
    cross3.zPosition = 1000
    cross3.alpha = 0

    addChild(cross1)
    addChild(cross2)
    addChild(cross3)
  }

  open func removeHeart() {
    life -= 1

    var heart : SKSpriteNode?
    switch life {
    case 2:
      heart = heart3
    case 1:
      heart = heart2
    case 0:
      heart = heart1
    default:
      print("something done broke")
      life = 0
    }

    heart?.run(SKAction.fadeOut(withDuration: 0.25))
    heart?.run(SKAction.scale(to: 3, duration: 0.3))
  }

  open func defeatedYellowDude() {
    ghostHunters -= 1

    cross1.setScale(3)
    cross1.run(SKAction.fadeIn(withDuration: 0.25))
    cross1.run(SKAction.scale(to: 1, duration: 0.3))
  }

  open func defeatedPurpleDude() {
    ghostHunters -= 1

    cross2.setScale(3)
    cross2.run(SKAction.fadeIn(withDuration: 0.25))
    cross2.run(SKAction.scale(to: 1, duration: 0.3))
  }

  open func defeatedGreenDude() {
    ghostHunters -= 1

    cross3.setScale(3)
    cross3.run(SKAction.fadeIn(withDuration: 0.25))
    cross3.run(SKAction.scale(to: 1, duration: 0.3))
  }

  open func getLifeRemaining() -> Int {
    return life
  }

  open func getGhostHuntersRemaining() -> Int {
    return ghostHunters
  }

  open func update(_ deltaTime: CFTimeInterval) {
    //TODO do something here... update some label possibly
  }
}
