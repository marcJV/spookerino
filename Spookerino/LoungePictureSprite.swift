//
//  LoungePictureSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/20/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class LoungePictureSprite : GlowableSprite, InteractiveSprite {
  fileprivate var goodGuySprite : SKSpriteNode!
  fileprivate var ghostGuySprite : SKSpriteNode!
  fileprivate var badGhostGuySprite : SKSpriteNode!

  fileprivate var badBackgroundSprite : SKSpriteNode!
  fileprivate var goodBackgroundSprite : SKSpriteNode!


  override open func setupSprite(_ imageName: String) {
    super.setupSprite(imageName)

    let root = getRootSprite()

    goodBackgroundSprite = SKSpriteNode(imageNamed: "lounge-picture-good-background")
    goodBackgroundSprite.zPosition = zPosition + 2
    root.addChild(goodBackgroundSprite)

    goodGuySprite = SKSpriteNode(imageNamed: "lounge-picture-good-dude")
    goodGuySprite.zPosition = zPosition + 3
    root.addChild(goodGuySprite)

    ghostGuySprite = SKSpriteNode(imageNamed: "lounge-picture-middle-dude")
    ghostGuySprite.zPosition = zPosition + 5
    ghostGuySprite.position = CGPoint(x: 3.5, y: 48.5)
    ghostGuySprite.alpha = 0
    root.addChild(ghostGuySprite)

    badGhostGuySprite = SKSpriteNode(imageNamed: "lounge-picure-bad-dude")
    badGhostGuySprite.zPosition = zPosition + 4
    badGhostGuySprite.position = CGPoint(x: 3.5, y: 48.5)
    badGhostGuySprite.colorBlendFactor = 0.85
    badGhostGuySprite.color = SKColor(red:1.00, green:0.34, blue:0.13, alpha:1.0)
    badGhostGuySprite.alpha = 0
    root.addChild(badGhostGuySprite)

    badBackgroundSprite = SKSpriteNode(imageNamed: "lounge-picture-bad-background")
    badBackgroundSprite.zPosition = zPosition + 1
    root.addChild(badBackgroundSprite)

    let foregroundFrameSprite = SKSpriteNode(imageNamed: imageName)
    foregroundFrameSprite.zPosition = zPosition + 4
    root.addChild(foregroundFrameSprite)
  }

  open func interact() {
    canInteract = false

    goodGuySprite.run(SKAction.fadeAlpha(to: 0.0, duration: 0.3))

    goodBackgroundSprite.run(SKAction.sequence([
      SKAction.wait(forDuration: 1.2),
      SKAction.fadeAlpha(to: 0.0, duration: 0.5)
      ]))

    ghostGuySprite.run(SKAction.sequence([
      SKAction.fadeAlpha(to: 1.0, duration: 0.5),
      SKAction.wait(forDuration: 0.9),
      SKAction.fadeOut(withDuration: 0.5)
      ]))

    badGhostGuySprite.run(SKAction.sequence([
      SKAction.wait(forDuration: 1.6),
      SKAction.fadeAlpha(to: 1.0, duration: 0.5)
      ]))
  }
}
