//
//  Updateable.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/17/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation

public protocol Updateable {
  func update(_ deltaTime: CFTimeInterval);
}
