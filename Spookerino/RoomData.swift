//
//  RoomData.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/16/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

public protocol RoomData {
  var backgroundTextureName : String { get }

  var roomType : RoomEnum { get }

  func getScaleForYPosition(_ yPosition : CGFloat) -> CGFloat

  func getChildren() -> [SKNode]

  func getDoors() -> [DoorSprite]

  func getGlowableSprites() -> [GlowableSprite]

  func getZones() -> [NPCZoneSprite]

  func getStartPosition(_ fromScene : RoomEnum) -> CGPoint

  func getBadGuyStartPosition() -> CGPoint
}

public enum RoomEnum {
  case foyerRoomType
  case libraryRoomType
  case loungeRoomType
  case zeroState
}
