//
//  LibraryRoomData.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/17/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class LibraryRoomData : RoomData {
  open var backgroundTextureName = "library-base"

  open let screenHeight = UIScreen.main.bounds.height

  fileprivate var children : [SKNode] = [SKNode]()

  open var doors = [DoorSprite]()

  open var glowable = [GlowableSprite]()

  fileprivate var zones = [NPCZoneSprite]()

  open var roomType: RoomEnum = RoomEnum.libraryRoomType

  open func getScaleForYPosition(_ yPosition : CGFloat) -> CGFloat {

    //    let screenPercentage = (screenHeight - yPosition) / screenHeight
    let measuredScreenPercentage = yPosition / screenHeight

    var characterScale : CGFloat = 1.0

    if measuredScreenPercentage < 0.35 {
      let tempScreenHeight = screenHeight * 0.35
      let tempScreenPercentage = (tempScreenHeight - yPosition) / tempScreenHeight

      characterScale = tempScreenPercentage * (0.85 - 0.6) + 0.6
    } else {
      characterScale = 0.6
    }

    return characterScale
  }

  open func getChildren() -> [SKNode] {
    if(children.count == 0) {
      setupChildren()
    }

    return children
  }

  open func getDoors() -> [DoorSprite] {
    return doors
  }

  open func getGlowableSprites() -> [GlowableSprite] {
    return glowable
  }

  open func getZones() -> [NPCZoneSprite] {
    return zones
  }

  open func getBadGuyStartPosition() -> CGPoint {
    return CGPoint(x: 499.5, y: 240.5)
  }

  fileprivate func setupChildren() {
    let rightDoor = DoorSprite()
    rightDoor.setupSprite("library-door-right")
    rightDoor.position = CGPoint(x: 862, y: 233)
    rightDoor.zPosition = 2
    rightDoor.toRoom = RoomEnum.foyerRoomType
    children.append(contentsOf: rightDoor.getChildren())
    doors.append(rightDoor)
    glowable.append(rightDoor)


    //Back wall stuff
    let fireplace = LibraryFireplaceSprite()
    fireplace.setupSprite("library-fireplace")
    fireplace.position = CGPoint(x: 507, y: 308)
    fireplace.zPosition = 1
    children.append(contentsOf: fireplace.getChildren())
    glowable.append(fireplace)

    let middlePicture = LibraryTopPictureSprite()
    middlePicture.setupSprite("library-picture-top")
    middlePicture.position = CGPoint(x: 507, y: 518)
    middlePicture.zPosition = 1
    children.append(contentsOf: middlePicture.getChildren())
    glowable.append(middlePicture)

    let rightLamp = LibraryLightSprite()
    rightLamp.setupSprite("library-lamp-right")
    rightLamp.position = CGPoint(x: 622, y: 518)
    rightLamp.zPosition = 1
    children.append(contentsOf: rightLamp.getChildren())
    glowable.append(rightLamp)

    let leftLamp = LibraryLightSprite()
    leftLamp.setupSprite("library-lamp-right")
    leftLamp.flipHorizontal()
    leftLamp.position = CGPoint(x: 397, y: 518)
    leftLamp.zPosition = 1
    children.append(contentsOf: leftLamp.getChildren())
    glowable.append(leftLamp)

    let rightBookCase = LibraryBookshelfSprite()
    rightBookCase.setupSprite("library-bookcase-left")
    rightBookCase.position = CGPoint(x: 705, y: 430)
    rightBookCase.zPosition = 1
    rightBookCase.flipHorizontal()
    children.append(contentsOf: rightBookCase.getChildren())
    glowable.append(rightBookCase)

    let leftBookCase = LibraryBookshelfSprite()
    leftBookCase.setupSprite("library-bookcase-left")
    leftBookCase.position = CGPoint(x: 310, y: 430)
    leftBookCase.zPosition = 1
    children.append(contentsOf: leftBookCase.getChildren())
    glowable.append(leftBookCase)

    // End back wall stuff

    let leftPicture = LibraryLeftPictureSprite()
    leftPicture.setupSprite("library-picture-left")
    leftPicture.position = CGPoint(x: 120, y: 448)
    children.append(contentsOf: leftPicture.getChildren())
    glowable.append(leftPicture)

    let sofa = LibrarySofaPossessableSprite()
    sofa.setupSprite("library-couch")
    sofa.position = CGPoint(x: 180, y: 148)
    sofa.zPosition = 2
    children.append(contentsOf: sofa.getChildren())
    glowable.append(sofa)

    let table = FloatyMcFloatyTable()
    table.setupSprite("library-table")
    table.position = CGPoint(x: 507, y: 90)
    table.zPosition = 75
    children.append(contentsOf: table.getChildren())
    glowable.append(table)

    setupZones()
  }

  open func setupZones() {
    let topRightZone = NPCZoneSprite(color: SKColor.clear, size: CGSize(width: 460, height: 150))
    topRightZone.anchorPoint = CGPoint()
    topRightZone.position = CGPoint(x: 525.5, y: 155.0)
    topRightZone.zPosition = 100
    topRightZone.alpha = 0.25
    topRightZone.quadrant = NPCZone.topRightQuad
    topRightZone.startPoint = CGPoint(x: 709.0, y: 260.5)

    let topLeftZone = NPCZoneSprite(color: SKColor.clear, size: CGSize(width: 450, height: 150))
    topLeftZone.anchorPoint = CGPoint()
    topLeftZone.position = CGPoint(x: 40, y: 155.0)
    topLeftZone.zPosition = 100
    topLeftZone.alpha = 0.25
    topLeftZone.quadrant = NPCZone.topLeftQuad
    topLeftZone.startPoint = CGPoint(x: 427.0, y: 270)

    let bottomRightZone = NPCZoneSprite(color: SKColor.clear, size: CGSize(width: 460, height: 150))
    bottomRightZone.anchorPoint = CGPoint()
    bottomRightZone.position = CGPoint(x: 525.5, y: 0)
    bottomRightZone.zPosition = 100
    bottomRightZone.alpha = 0.25
    bottomRightZone.startPoint = CGPoint(x: 824.5, y: 99.5)
    bottomRightZone.quadrant = NPCZone.bottomRightQuad

    let bottomLeftZone = NPCZoneSprite(color: SKColor.clear, size: CGSize(width: 450, height: 150))
    bottomLeftZone.anchorPoint = CGPoint()
    bottomLeftZone.position = CGPoint(x: 40, y: 0)
    bottomLeftZone.zPosition = 100
    bottomLeftZone.alpha = 0.25
    bottomLeftZone.startPoint = CGPoint(x: 331.5, y: 73.0)
    bottomLeftZone.quadrant = NPCZone.bottomLeftQuad

    children.append(topRightZone)
    zones.append(topRightZone)
    children.append(topLeftZone)
    zones.append(topLeftZone)
    children.append(bottomRightZone)
    zones.append(bottomRightZone)
    children.append(bottomLeftZone)
    zones.append(bottomLeftZone)

    // Top Left Zone Paths
    var path = UIBezierPath()
    path.move(to: topLeftZone.startPoint)
    path.addQuadCurve(to: bottomRightZone.startPoint, controlPoint: CGPoint(x: 769.0, y: 320.0))

    topLeftZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topLeftZone.startPoint)
    path.addLine(to: CGPoint(x: 334.5, y: 148.5))
    path.addLine(to: CGPoint(x: 389.5, y: 8.5))
    path.addLine(to: bottomRightZone.startPoint)

    topLeftZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topLeftZone.startPoint)
    path.addQuadCurve(to: CGPoint(x: 707.0, y: 175.0), controlPoint: CGPoint(x: 646.5, y: 273.0))
    path.addLine(to: topRightZone.startPoint)

    topLeftZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topLeftZone.startPoint)
    path.addLine(to: CGPoint(x: 345.0, y: 169.0))
    path.addLine(to: bottomLeftZone.startPoint)

    topLeftZone.roomPaths.append(path.cgPath)

    // Top Right Zone Paths
    path = UIBezierPath()
    path.move(to: topRightZone.startPoint)
    path.addLine(to: CGPoint(x: 561.5, y: 260.0))
    path.addLine(to: topLeftZone.startPoint)

    topRightZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topRightZone.startPoint)
    path.addLine(to: CGPoint(x: 720.0, y: 97.5))
    path.addLine(to: bottomRightZone.startPoint)

    topRightZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topRightZone.startPoint)
    path.addQuadCurve(to: bottomLeftZone.startPoint, controlPoint: CGPoint(x: 933.0, y: 8.0))

    topRightZone.roomPaths.append(path.cgPath)

    // Bottom Left Zone Paths
    path = UIBezierPath()
    path.move(to: bottomLeftZone.startPoint)
    path.addLine(to: CGPoint(x: 349.5, y: 188.5))
    path.addLine(to: topLeftZone.startPoint)

    bottomLeftZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: bottomLeftZone.startPoint)
    path.addLine(to: CGPoint(x: 371.5, y: 243.0))
    path.addLine(to: topRightZone.startPoint)

    bottomLeftZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: bottomLeftZone.startPoint)
    path.addLine(to: CGPoint(x: 505.0, y: 124.0))
    path.addLine(to: bottomRightZone.startPoint)

    bottomLeftZone.roomPaths.append(path.cgPath)

    // Bottom Right Zone Paths
    path = UIBezierPath()
    path.move(to: bottomRightZone.startPoint)
    path.addQuadCurve(to: topLeftZone.startPoint, controlPoint: CGPoint(x: 762.5, y: 252.5))

    bottomRightZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: bottomRightZone.startPoint)
    path.addLine(to: CGPoint(x: 658.0, y: 192.0))
    path.addLine(to: topRightZone.startPoint)

    bottomRightZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: bottomRightZone.startPoint)
    path.addQuadCurve(to: bottomLeftZone.startPoint, controlPoint: CGPoint(x: 271.0, y: 8.0))

    bottomRightZone.roomPaths.append(path.cgPath)
  }

  //Only one way in for now, ignore fromScene and set start point
  open func getStartPosition(_ fromScene : RoomEnum) -> CGPoint {
    return CGPoint(x: 815, y: 233)
  }
}
