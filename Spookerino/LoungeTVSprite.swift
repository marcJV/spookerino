//
//  LoungeTVSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/20/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class LoungeTVSprite : GlowableSprite, InteractiveSprite {
  fileprivate var creepyFaceSprite : SKSpriteNode!
  fileprivate var creepyHandSprite : SKSpriteNode!
  fileprivate var creepyCrackSprite : SKSpriteNode!

  override open func setupSprite(_ imageName: String) {
    super.setupSprite(imageName)

    creepyFaceSprite = SKSpriteNode(imageNamed: "lounge-spooky-face")
    creepyFaceSprite.position = CGPoint(x: 10, y: 3)
    creepyFaceSprite.zPosition = zPosition + 1
    creepyFaceSprite.alpha = 0
    getRootSprite().addChild(creepyFaceSprite)

    creepyHandSprite = SKSpriteNode(imageNamed: "lounge-spooky-hand")
    creepyHandSprite.position = CGPoint(x: -20, y: -5)
    creepyHandSprite.zPosition = zPosition + 1
    creepyHandSprite.alpha = 0
    getRootSprite().addChild(creepyHandSprite)

    creepyCrackSprite = SKSpriteNode(imageNamed: "lounge-glass-break")
    creepyCrackSprite.position = CGPoint(x: -25, y: 7)
    creepyCrackSprite.zPosition = zPosition + 2
    creepyCrackSprite.alpha = 0
    getRootSprite().addChild(creepyCrackSprite)
  }

  open func interact() {
    canInteract = false

    creepyFaceSprite.run(SKAction.fadeIn(withDuration: 3.0))

    creepyHandSprite.run(SKAction.sequence([
      SKAction.wait(forDuration: 4.0),
      SKAction.fadeIn(withDuration: 0.1)
      ]))

    creepyCrackSprite.run(SKAction.sequence([
      SKAction.wait(forDuration: 4.5),
      SKAction.fadeIn(withDuration: 6)
      ]))
  }
}
