//
//  DoorSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/17/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class DoorSprite : GlowableSprite {
  open var toRoom : RoomEnum = RoomEnum.zeroState

  open override func setupSprite(_ imageName: String) {
    glowColor = SKColor.white

    super.setupSprite(imageName)

    timerDuration = 1
  }
}
