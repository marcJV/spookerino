//
//  LoungeKnobPossessableSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/21/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class LoungeKnobPossessableSprite : GlowableSprite, PossessableSprite {
  open var possessable: Bool = false

  fileprivate var knobOpenTexture : SKTexture!

  open var sofaSprite : LoungeSofaSprite!

  override open func setupSprite(_ imageName: String) {
    glowColor = UIColor.blue

    super.setupSprite(imageName)

    knobOpenTexture = SKTexture(imageNamed: "lounge-knob-activated")
  }

  open func possess() {
    possessable = false
    canInteract = false

    getRootSprite().texture = knobOpenTexture
    getRootSprite().size = CGSize(width: 23, height: 46)

    sofaSprite.enginesOff()
  }
}
