//
//  FoyerFireLightSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/21/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class FoyerFireLightSprite : GlowableSprite, InteractiveSprite {
  fileprivate var fireSmallSprite : SKSpriteNode!
  fileprivate var fireMediumSprite : SKSpriteNode!
  fileprivate var fireLargeSprite : SKSpriteNode!
  
  override open func setupSprite(_ imageName: String) {
    super.setupSprite(imageName)

    let firePosition = CGPoint(x: 0, y: getRootSprite().size.height / 2 - 7)
    let fireScale : CGFloat = 0.5

    let root = getRootSprite()

    fireSmallSprite = SKSpriteNode(imageNamed: "library-fire-small")
    fireSmallSprite.position = position
    fireSmallSprite.zPosition = zPosition + 1
    fireSmallSprite.anchorPoint = CGPoint(x: 0.5, y: 0)
    fireSmallSprite.position = firePosition
    fireSmallSprite.alpha = 0
    fireSmallSprite.setScale(fireScale)
    root.addChild(fireSmallSprite)

    fireMediumSprite = SKSpriteNode(imageNamed: "library-fire-medium")
    fireMediumSprite.position = position
    fireMediumSprite.zPosition = zPosition + 1
    fireMediumSprite.anchorPoint = CGPoint(x: 0.5, y: 0)
    fireMediumSprite.position = firePosition
    fireMediumSprite.alpha = 0
    fireMediumSprite.setScale(fireScale)
    root.addChild(fireMediumSprite)

    fireLargeSprite = SKSpriteNode(imageNamed: "library-fire-large")
    fireLargeSprite.position = position
    fireLargeSprite.zPosition = zPosition + 1
    fireLargeSprite.anchorPoint = CGPoint(x: 0.5, y: 0)
    fireLargeSprite.position = firePosition
    fireLargeSprite.alpha = 0
    fireLargeSprite.setScale(fireScale)
    root.addChild(fireLargeSprite)
  }

  open func interact() {
    canInteract = false

    let fadeInAction = SKAction.fadeIn(withDuration: 0.1)
    let fadeOutAction = SKAction.fadeOut(withDuration: 0.1)

    let scaleChange1 = SKAction.scaleX(to: -1, duration: 0)
    let scaleChange2 = SKAction.scaleX(to: 1, duration: 0)

    let waitAction = SKAction.wait(forDuration: 0.25)

    fireSmallSprite.alpha = 1

    fireSmallSprite.run(SKAction.repeatForever(SKAction.sequence([
      scaleChange1,
      waitAction,
      scaleChange2,
      waitAction
      ])))

    fireMediumSprite.run(SKAction.repeatForever(SKAction.sequence([
      scaleChange1,
      waitAction,
      scaleChange2,
      waitAction
      ])))

    fireLargeSprite.run(SKAction.repeatForever(SKAction.sequence([
      scaleChange1,
      waitAction,
      scaleChange2,
      waitAction
      ])))

    fireMediumSprite.run(SKAction.repeatForever(SKAction.sequence([
      waitAction,
      fadeInAction,
      waitAction,
      waitAction,
      waitAction,
      fadeOutAction,
      ])))

    fireLargeSprite.run(SKAction.repeatForever(SKAction.sequence([
      waitAction,
      waitAction,
      fadeInAction,
      waitAction,
      fadeOutAction,
      waitAction,
      ])))
  }
}
