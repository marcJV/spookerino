//
//  FloatyMcFloatyTable.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/20/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class FloatyMcFloatyTable : GlowableSprite, InteractiveSprite {

  open func interact() {
    canInteract = false

    let tableUpAction = SKAction.move(by: CGVector(dx: 0, dy: 80), duration: 0.7)
    let tableDownAction = SKAction.move(by: CGVector(dx: 0, dy: -80), duration: 0.7)

    getRootSprite().run(SKAction.repeatForever(SKAction.sequence([
      tableUpAction,
      tableDownAction,
      SKAction.wait(forDuration: 0.25),
      tableUpAction,
      SKAction.wait(forDuration: 0.5),
      SKAction.rotate(toAngle: CGFloat.pi, duration: 0.8),
      tableDownAction,
      SKAction.wait(forDuration: 0.25),
      tableUpAction,
      tableDownAction,
      tableUpAction,
      SKAction.wait(forDuration: 0.25),
      SKAction.rotate(toAngle: -CGFloat.pi, duration: 0.8),
      tableDownAction,
      tableUpAction,
      tableDownAction,
      SKAction.wait(forDuration: 0.25),
      tableUpAction,
      SKAction.wait(forDuration: 0.5),
      SKAction.rotate(toAngle: CGFloat(0), duration: 0.8),
      tableDownAction,
      SKAction.wait(forDuration: 0.25),
      ])))
  }
}
