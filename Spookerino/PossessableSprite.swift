//
//  PossessableSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/18/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

public protocol PossessableSprite {
  var possessable : Bool { get set }

  func possess()
}