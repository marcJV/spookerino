//
//  TouchManager.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/17/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

public protocol Touchable {
  func beganTouchAtPoint(_ point : CGPoint, timeInterval : TimeInterval,
                          longPressCompletion : @escaping LongPressCompletionNotifier)
  func endedTouchAtPoint(_ point : CGPoint, timeInterval : TimeInterval)
  func clickedAtPoint(_ point : CGPoint)
  func cancelledTouchAtPoint(_ point : CGPoint)
}
