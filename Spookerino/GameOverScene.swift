//
//  GameOverScene.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/21/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

class GameOverScene: SKScene {
  var lastTime :CFTimeInterval = 0
  let ghostDude = GhostSprite(imageNamed: "dude")

  override func didMove(to view: SKView) {
    anchorPoint = CGPoint()
    let background = SKSpriteNode(imageNamed: "you-lose-scene")
    background.anchorPoint = CGPoint()
    background.zPosition = 1

    ghostDude.position = CGPoint(x: 300, y: 100)
    ghostDude.zRotation = 1.5708
    ghostDude.zPosition = 2
    ghostDude.showTimer = false
    ghostDude.hideTimerBecauseThisIsAHack()
    ghostDude.setScale(0.85)

    let beachballOfSadness = SKSpriteNode(imageNamed: "beach-ball")
    beachballOfSadness.position = CGPoint(x: 270, y: 200)
    beachballOfSadness.zPosition = 3

    beachballOfSadness.run(SKAction.repeatForever(SKAction.rotate(byAngle: CGFloat.pi, duration:0.5)))

    beachballOfSadness.run(SKAction.repeatForever(SKAction.sequence([
      SKAction.move(by: CGVector(dx: 0, dy: 300), duration: 0.7),
      SKAction.move(by: CGVector(dx: 0, dy: -300), duration: 0.7)
      ])))

    addChild(background)
    addChild(ghostDude)
    addChild(beachballOfSadness)
  }
}
