//
//  LoungeFirePlaceSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/20/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class LoungeFireplaceSprite : GlowableSprite, InteractiveSprite {
  fileprivate var fireSmallSprite : SKSpriteNode!
  fileprivate var fireMediumSprite : SKSpriteNode!
  fileprivate var fireLargeSprite : SKSpriteNode!
  let fireScaleX : CGFloat = 0.65
  let fireScaleY : CGFloat = 1.5

  override open func setupSprite(_ imageName: String) {
    super.setupSprite(imageName)

    let root = getRootSprite()
    let firePosition = CGPoint(x: 30, y: -18)

    fireSmallSprite = SKSpriteNode(imageNamed: "library-fire-small")
    fireSmallSprite.position = position
    fireSmallSprite.zPosition = zPosition + 1
    fireSmallSprite.anchorPoint = CGPoint(x: 0.5, y: 0)
    fireSmallSprite.position = firePosition
    fireSmallSprite.alpha = 0
    fireSmallSprite.xScale = fireScaleX
    fireSmallSprite.yScale = fireScaleY
    root.addChild(fireSmallSprite)

    fireMediumSprite = SKSpriteNode(imageNamed: "library-fire-medium")
    fireMediumSprite.position = position
    fireMediumSprite.zPosition = zPosition + 1
    fireMediumSprite.anchorPoint = CGPoint(x: 0.5, y: 0)
    fireMediumSprite.position = firePosition
    fireMediumSprite.alpha = 0
    fireMediumSprite.xScale = fireScaleX
    fireMediumSprite.yScale = fireScaleY
    root.addChild(fireMediumSprite)

    fireLargeSprite = SKSpriteNode(imageNamed: "library-fire-large")
    fireLargeSprite.position = position
    fireLargeSprite.zPosition = zPosition + 1
    fireLargeSprite.anchorPoint = CGPoint(x: 0.5, y: 0)
    fireLargeSprite.position = firePosition
    fireLargeSprite.alpha = 0
    fireLargeSprite.xScale = fireScaleX
    fireLargeSprite.yScale = fireScaleY
    root.addChild(fireLargeSprite)
  }

  open func interact() {
    canInteract = false

    let fadeInAction = SKAction.fadeIn(withDuration: 0.1)
    let fadeOutAction = SKAction.fadeOut(withDuration: 0.1)

    let scaleChange1 = SKAction.scaleX(to: fireScaleX * -1, duration: 0)
    let scaleChange2 = SKAction.scaleX(to: fireScaleX, duration: 0)

    let waitAction = SKAction.wait(forDuration: 0.25)

    fireSmallSprite.alpha = 1

    fireSmallSprite.run(SKAction.repeatForever(SKAction.sequence([
      scaleChange1,
      waitAction,
      scaleChange2,
      waitAction
      ])))

    fireMediumSprite.run(SKAction.repeatForever(SKAction.sequence([
      scaleChange1,
      waitAction,
      scaleChange2,
      waitAction
      ])))

    fireLargeSprite.run(SKAction.repeatForever(SKAction.sequence([
      scaleChange1,
      waitAction,
      scaleChange2,
      waitAction
      ])))

    fireMediumSprite.run(SKAction.repeatForever(SKAction.sequence([
      waitAction,
      fadeInAction,
      waitAction,
      waitAction,
      waitAction,
      fadeOutAction,
      ])))

    fireLargeSprite.run(SKAction.repeatForever(SKAction.sequence([
      waitAction,
      waitAction,
      fadeInAction,
      waitAction,
      fadeOutAction,
      waitAction,
      ])))    
  }
}
