//
//  RoomManager.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/16/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

public typealias RoomChanger = ((_ fromRoom : RoomEnum, _ toRoom : RoomEnum) -> ())
public typealias EnemyDefeatedNotifier = () -> ()
public typealias DamageTakenNotifier = (() -> ())
public typealias LongPressCompletionNotifier = () -> ()

open class RoomManager : SKSpriteNode, Touchable, Updateable {
  var currentRoom : Room!

  fileprivate var foyerRoom : Room = Room()
  fileprivate var loungeRoom : Room = Room()
  fileprivate var libraryRoom : Room = Room()

  fileprivate var yellowDude = BadGuySprite(imageNamed: "yellow-dude")
  fileprivate var greenDude = BadGuySprite(imageNamed: "green-dude")
  fileprivate var purpleDude = BadGuySprite(imageNamed: "purple-dude")

  fileprivate var roomChanger : RoomChanger!

  open var yellowDefeated : EnemyDefeatedNotifier!
  open var purpleDefeated : EnemyDefeatedNotifier!
  open var greenDefeated : EnemyDefeatedNotifier!

  open func setupRooms(_ damageTakenNotifier :  @escaping DamageTakenNotifier) {
    roomChanger = { (fromRoom : RoomEnum, toRoom : RoomEnum) in
      let oldRoom = self.currentRoom

      switch toRoom {
      case .foyerRoomType:
        self.currentRoom = self.foyerRoom
      case .loungeRoomType:
        self.currentRoom = self.loungeRoom
      case .libraryRoomType:
        self.currentRoom = self.libraryRoom
      default:
        print("No Room to switch to!")
      }

      if oldRoom != self.currentRoom {
        self.currentRoom.roomChanger = nil
        self.currentRoom.roomChanger = self.roomChanger

        oldRoom?.removeFromParent()
        self.addChild(self.currentRoom)

        self.currentRoom.didMoveToRoom(fromRoom)
      }
    }

    anchorPoint = CGPoint()
    //Set up all room data here
    foyerRoom.setRoomData(FoyerRoomData())
    foyerRoom.enemyDefeated = ({
      self.greenDefeated()
    })

    libraryRoom.setRoomData(LibraryRoomData())
    libraryRoom.enemyDefeated = ({
      self.purpleDefeated()
    })

    loungeRoom.setRoomData(LoungeRoomData())
    loungeRoom.enemyDefeated = ({
      self.yellowDefeated()
    })

    foyerRoom.damageTakenNotifier = damageTakenNotifier
    libraryRoom.damageTakenNotifier = damageTakenNotifier
    loungeRoom.damageTakenNotifier = damageTakenNotifier

    //TODO reset to foyer after test is over
    currentRoom = foyerRoom

    addChild(currentRoom)

    currentRoom.didMoveToRoom(RoomEnum.zeroState)
    currentRoom.roomChanger = roomChanger

    loungeRoom.addBaddieInStartPosition(yellowDude)
    foyerRoom.addBaddieInStartPosition(greenDude)
    libraryRoom.addBaddieInStartPosition(purpleDude)
  }

  open func update(_ deltaTime: CFTimeInterval) {
    foyerRoom.update(deltaTime)
    loungeRoom.update(deltaTime)
    libraryRoom.update(deltaTime)
  }

  open func beganTouchAtPoint(_ point: CGPoint, timeInterval : TimeInterval, longPressCompletion : @escaping LongPressCompletionNotifier) {
    currentRoom.beganTouchAtPoint(point, timeInterval: timeInterval, longPressCompletion : longPressCompletion)
  }

  open func clickedAtPoint(_ clickPoint : CGPoint) {
    currentRoom.clickedAtPoint(clickPoint)
  }

  open func endedTouchAtPoint(_ point: CGPoint, timeInterval : TimeInterval) {
    currentRoom.endedTouchAtPoint(point, timeInterval: timeInterval)
  }

  open func cancelledTouchAtPoint(_ point: CGPoint) {
    currentRoom.cancelledTouchAtPoint(point)
  }
}
