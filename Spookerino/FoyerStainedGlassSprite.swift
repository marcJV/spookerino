//
//  FoyerStainedGlassSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/20/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class FoyerStainedGlassSprite : GlowableSprite, InteractiveSprite {
  fileprivate var scaryFaceFrameOne : SKSpriteNode!
  fileprivate var scaryFaceFrameTwo : SKSpriteNode!

  override open func setupSprite(_ imageName: String) {
    super.setupSprite(imageName)

    scaryFaceFrameOne = SKSpriteNode(imageNamed: "foyer-stained-glass-face")
    scaryFaceFrameOne.position = position
    scaryFaceFrameOne.zPosition = zPosition + 1
    scaryFaceFrameOne.alpha = 0
    getRootSprite().addChild(scaryFaceFrameOne)

    scaryFaceFrameTwo = SKSpriteNode(imageNamed: "foyer-stained-glass-face-two")
    scaryFaceFrameTwo.position = position
    scaryFaceFrameTwo.zPosition = zPosition + 2
    scaryFaceFrameTwo.alpha = 0
    getRootSprite().addChild(scaryFaceFrameTwo)
  }

  open func interact() {
    canInteract = false

    scaryFaceFrameOne.run(SKAction.fadeIn(withDuration: 0.25), completion: {
      self.scaryFaceFrameTwo.run(SKAction.repeatForever(SKAction.sequence([
        SKAction.fadeOut(withDuration: 0.0),
        SKAction.wait(forDuration: 0.1),
        SKAction.fadeIn(withDuration: 0.0),
        SKAction.wait(forDuration: 0.1)
        ])))
    })
  }
}
