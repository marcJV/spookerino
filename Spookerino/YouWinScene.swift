//
//  YouWinScene.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/21/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

class YouWinScene: SKScene {
  var lastTime :CFTimeInterval = 0
  let ghostDude = GhostSprite(imageNamed: "dude")

  override func didMove(to view: SKView) {
    anchorPoint = CGPoint()
    let background = SKSpriteNode(imageNamed: "you-win-scene")
    background.anchorPoint = CGPoint()
    background.zPosition = 1

    ghostDude.position = CGPoint(x: 200, y: 200)
    ghostDude.zPosition = 2
    ghostDude.showTimer = false
    ghostDude.hideTimerBecauseThisIsAHack()
    ghostDude.setScale(0.65)

    let yellowGhost = SKSpriteNode(imageNamed: "yellow-ghost")
    yellowGhost.position = CGPoint(x: 250, y: 500)
    yellowGhost.zPosition = 2
    yellowGhost.setScale(0.65)

    let greenGhost = SKSpriteNode(imageNamed: "green-ghost")
    greenGhost.position = CGPoint(x: 550, y: 225)
    greenGhost.zPosition = 2
    greenGhost.setScale(0.65)
    greenGhost.xScale = greenGhost.xScale * -1

    let purpleGhost = SKSpriteNode(imageNamed: "purple-ghost")
    purpleGhost.position = CGPoint(x: 600, y: 450)
    purpleGhost.zPosition = 2
    purpleGhost.setScale(0.65)
    purpleGhost.xScale = purpleGhost.xScale * -1

    let beachballOfHappiness = SKSpriteNode(imageNamed: "beach-ball")
    beachballOfHappiness.anchorPoint = CGPoint()
    beachballOfHappiness.position = CGPoint(x: 410, y: 360)
    beachballOfHappiness.zPosition = 3
    beachballOfHappiness.setScale(0.85)

    beachballOfHappiness.run(SKAction.repeatForever(SKAction.rotate(byAngle: CGFloat.pi, duration:1.0)))

    addChild(background)
    addChild(ghostDude)
    addChild(yellowGhost)
    addChild(greenGhost)
    addChild(purpleGhost)
    addChild(beachballOfHappiness)
  }
}
