//
//  LibraryTopPictureSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/20/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class LibraryTopPictureSprite : GlowableSprite, InteractiveSprite {
  fileprivate var jeepSprite : SKSpriteNode!

  override open func setupSprite(_ imageName: String) {
    super.setupSprite(imageName)

    jeepSprite = SKSpriteNode(imageNamed: "library-picture-jeep")
    jeepSprite.position = position
    jeepSprite.zPosition = zPosition + 1
    getRootSprite().addChild(jeepSprite)
  }

  open func interact() {
    canInteract = false

    //Totally got this from the starter app
    let action = SKAction.rotate(byAngle: CGFloat(M_PI), duration:0.5)

    jeepSprite.run(SKAction.repeatForever(action))

  }
}
