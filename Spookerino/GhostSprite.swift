//
//  MoveableSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/16/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

/*
 * BEWARE OF SPOOKY PHYSICS HERE
 */
class GhostSprite : ScaleableSprite {

  fileprivate var spookyEasing : CGFloat = 0.1
  fileprivate var spookyDestination : CGPoint = CGPoint.zero
  fileprivate let timer = SKShapeNode(circleOfRadius: 30)
  fileprivate let timerCrop = SKCropNode()
  internal var showTimer = false
  fileprivate var currentTimerInterval : TimeInterval = 0
  fileprivate var timerDuration : TimeInterval = 0
  fileprivate let timerCenter : CGPoint
  fileprivate let timerRadius : CGFloat = 30
  internal var touchable = true

  fileprivate var longPressCompletionNotifier : LongPressCompletionNotifier?

  override init(texture: SKTexture?, color: UIColor, size: CGSize) {
    timerCenter = CGPoint(x: size.width / 4 + 20, y: size.height / 3 - 20)

    super.init(texture: texture, color: SKColor(red:1.00, green:0.34, blue:0.13, alpha:1.0), size: size)

    colorBlendFactor = 0.85
    timer.fillColor = SKColor(red:1.00, green:0.34, blue:0.13, alpha:1.0)
    timer.position = timerCenter
    timer.zPosition = 998
    timer.zRotation = (CGFloat.pi / CGFloat(180)) * 90

    addChild(timer)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func didEnterRoom() {
    touchable = false
    alpha = 0.7

    let delayTime = DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(deadline: delayTime) {
      self.removeEthereal()
    }
  }

  func setEthereal() {
    touchable = false
    let fadeOutAlphaAction = SKAction.fadeAlpha(to: 0.70, duration: 0.085)
    let fadeInAlphaAction = SKAction.fadeAlpha(to: 1.0, duration: 0.085)

    run(SKAction.repeatForever(SKAction.sequence([fadeOutAlphaAction, fadeInAlphaAction])))

    let delayTime = DispatchTime.now() + Double(Int64(2.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(deadline: delayTime) {
      self.removeEthereal()
    }
  }

  func removeEthereal() {
    removeAllActions()

    run(SKAction.fadeAlpha(to: 1.0, duration: 0.3), completion: {
      self.touchable = true
    }) 
  }

  func hideTimerBecauseThisIsAHack() {
    timer.removeFromParent()
  }

  func updateSpookyDestination(_ spookyDestination : CGPoint) {
    self.spookyDestination = spookyDestination
    let distance = sqrt(pow((spookyDestination.x - position.x), 2) + pow((spookyDestination.y - position.y), 2))

    //Long distances are spooky for the ghost, so it moves slower
    if(distance > 700) {
      spookyEasing = 0.04
    } else if(distance > 400) {
      spookyEasing = 0.07
    } else if(distance < 150){
      spookyEasing = 0.4
    } else {
      spookyEasing = 0.1
    }

    //Toggle heading
    if (facingRight && position.x > spookyDestination.x) || (!facingRight && position.x < spookyDestination.x) {
      facingRight = !facingRight
    }
  }

  func updatePosition(_ newPosition : CGPoint) {
    position = newPosition
    spookyDestination = newPosition
  }

  func startTimer(_ duration : TimeInterval, longPressNotifier : @escaping LongPressCompletionNotifier, timerColor : SKColor) {
    currentTimerInterval = 0
    timerDuration = duration
    timer.fillColor = timerColor
    timer.strokeColor = timerColor
    showTimer = true
    self.longPressCompletionNotifier = longPressNotifier
  }

  func cancelTimer() {
    longPressCompletionNotifier = nil
    showTimer = false
  }

  internal override func update(_ deltaTime: CFTimeInterval) {
    super.update(deltaTime)

    let distance = sqrt(pow((spookyDestination.x - position.x), 2) + pow((spookyDestination.y - position.y), 2))

    //1 is the spookiest of the numbers
    if(distance > 1) {
      let directionX = (spookyDestination.x - position.x)
      let directionY = (spookyDestination.y - position.y)

      position.x += directionX * spookyEasing
      position.y += directionY * spookyEasing
    } else {
      //Too spooked, go directly to destination
      position = spookyDestination;
    }

    if(showTimer && currentTimerInterval < timerDuration) {
      timer.alpha = 1

      let path = UIBezierPath()
      path.move(to: timerCenter)

      var startpoint = timerCenter
      startpoint.y += timerRadius

      currentTimerInterval += deltaTime

      let endAngle = CGFloat((currentTimerInterval / timerDuration) * 360)
      let radians = (CGFloat.pi / CGFloat(180.0)) * endAngle

      path.addArc(withCenter: timerCenter, radius: timerRadius, startAngle: 0, endAngle: radians, clockwise: true)
      path.addLine(to: timerCenter)

      timer.path = path.cgPath

      if currentTimerInterval >= timerDuration {
        longPressCompletionNotifier?()
      }
    } else {
      timer.alpha = 0
      showTimer = false
    }
  }

  internal func performPossessAnimation(_ completion: () -> ()) {
    touchable = false
    
    completion()
  }
}
