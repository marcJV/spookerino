//
//  FoyerRoomData.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/17/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class FoyerRoomData : RoomData {
  open var roomType: RoomEnum = RoomEnum.foyerRoomType

  open var backgroundTextureName = "foyer-base"

  open let screenHeight = UIScreen.main.bounds.height

  fileprivate var children : [SKNode] = [SKNode]()

  fileprivate var doors = [DoorSprite]()

  fileprivate var glowable = [GlowableSprite]()

  fileprivate var zones = [NPCZoneSprite]()

  open func getScaleForYPosition(_ yPosition : CGFloat) -> CGFloat {
    let measuredScreenPercentage = yPosition / screenHeight

    var characterScale : CGFloat = 1.0

    if measuredScreenPercentage < 0.35 {
      let tempScreenHeight = screenHeight * 0.35
      let tempScreenPercentage = (tempScreenHeight - yPosition) / tempScreenHeight

      characterScale = tempScreenPercentage * (0.85 - 0.6) + 0.6
    } else if measuredScreenPercentage < 0.73 {
      let tempScreenHeight = (screenHeight * 0.73) - (screenHeight * 0.35)
      let fixedYPos = yPosition - (screenHeight * 0.35)
      let tempScreenPercentage = (tempScreenHeight - fixedYPos) / tempScreenHeight


      characterScale = tempScreenPercentage * (0.6 - 0.4) + 0.4
    } else {
      characterScale = 0.4
    }

    return characterScale
  }

  open func getChildren() -> [SKNode] {
    if(children.count == 0) {
      setupChildren()
    }

    return children
  }

  open func getDoors() -> [DoorSprite] {
    return doors
  }

  open func getGlowableSprites() -> [GlowableSprite] {
    return glowable
  }

  open func getZones() -> [NPCZoneSprite] {
    return zones
  }

  open func getBadGuyStartPosition() -> CGPoint {
    if(getZones().count > 0) {
      let zone =  Int(arc4random_uniform(UInt32(getZones().count)))
      return getZones()[zone].startPoint
    }

    return CGPoint()
  }

  fileprivate func setupChildren() {
    let rails = SKSpriteNode(imageNamed: "foyer-railing")
    rails.position = CGPoint(x: 512, y: 410)
    rails.zPosition = 45
    children.append(rails)

    let leftDoor = DoorSprite()
    leftDoor.setupSprite("foyer-door-left")
    leftDoor.toRoom = RoomEnum.libraryRoomType
    leftDoor.position = CGPoint(x: 60, y: 280)
    leftDoor.zPosition = 2
    children.append(contentsOf: leftDoor.getChildren())
    doors.append(leftDoor)
    glowable.append(leftDoor)

    let rightDoor = DoorSprite()
    rightDoor.setupSprite("foyer-door-right")
    rightDoor.toRoom = RoomEnum.loungeRoomType
    rightDoor.position = CGPoint(x: 962, y: 280)
    rightDoor.zPosition = 2
    children.append(contentsOf: rightDoor.getChildren())
    doors.append(rightDoor)
    glowable.append(rightDoor)

    let stainedGlass = FoyerStainedGlassSprite()
    stainedGlass.setupSprite("foyer-stained-glass")
    stainedGlass.position = CGPoint(x: 512, y: 640)
    stainedGlass.zPosition = 2
    children.append(contentsOf: stainedGlass.getChildren())
    glowable.append(stainedGlass)

    let leftLeftLamp = FoyerFireLightSprite()
    leftLeftLamp.setupSprite("foyer-lamp-left")
    leftLeftLamp.position = CGPoint(x: 225, y: 560)
    leftLeftLamp.zPosition = 1
    children.append(contentsOf: leftLeftLamp.getChildren())
    glowable.append(leftLeftLamp)

    let leftRightLamp = FoyerLightSprite()
    leftRightLamp.setupSprite("foyer-lamp-left")
    leftRightLamp.position = CGPoint(x: 310, y: 620)
    leftRightLamp.zPosition = 1
    children.append(contentsOf: leftRightLamp.getChildren())
    glowable.append(leftRightLamp)

    let rightLeftLamp = FoyerFireLightSprite()
    rightLeftLamp.setupSprite("foyer-lamp-right")
    rightLeftLamp.position = CGPoint(x: 710, y: 620)
    rightLeftLamp.zPosition = 1
    children.append(contentsOf: rightLeftLamp.getChildren())
    glowable.append(rightLeftLamp)

    let rightRightLamp = FoyerLightSprite()
    rightRightLamp.setupSprite("foyer-lamp-right")
    rightRightLamp.position = CGPoint(x: 800, y: 560)
    rightRightLamp.zPosition = 1
    children.append(contentsOf: rightRightLamp.getChildren())
    glowable.append(rightRightLamp)

    let doorMat = FoyerDoorMatSprite()
    doorMat.setupSprite("foyer-doormat")
    doorMat.position = CGPoint(x: 512, y: 60)
    doorMat.zPosition = 1
    children.append(contentsOf: doorMat.getChildren())
    glowable.append(doorMat)

    let organ = FoyerOrganPossessableSprite()
    organ.setupSprite("foyer-organ")
    organ.position = CGPoint(x: 512, y: 372)
    organ.zPosition = 1
    children.append(contentsOf: organ.getChildren())
    glowable.append(organ)

    setupZones()
  }

  fileprivate func setupZones() {
    let topStairsZone = NPCZoneSprite(color: SKColor.clear, size: CGSize(width: 280, height: 150))
    topStairsZone.anchorPoint = CGPoint()
    topStairsZone.position = CGPoint(x: 382.0, y: 480)
    topStairsZone.zPosition = 100
    topStairsZone.alpha = 0.25
    topStairsZone.quadrant = NPCZone.topRightQuad
    topStairsZone.startPoint = CGPoint(x: 521.5, y: 560.0)

    let middleStairsZone = NPCZoneSprite(color: SKColor.clear, size: CGSize(width: 280, height: 200))
    middleStairsZone.anchorPoint = CGPoint()
    middleStairsZone.position = CGPoint(x: 382.0, y: 200)
    middleStairsZone.zPosition = 100
    middleStairsZone.alpha = 0.25
    middleStairsZone.quadrant = NPCZone.topRightQuad
    middleStairsZone.startPoint = CGPoint(x: 504.5, y: 343.0)

    let bottomLeftZone = NPCZoneSprite(color: SKColor.clear, size: CGSize(width: 280, height: 200))
    bottomLeftZone.anchorPoint = CGPoint()
    bottomLeftZone.position = CGPoint(x: 0, y: 0)
    bottomLeftZone.zPosition = 100
    bottomLeftZone.alpha = 0.25
    bottomLeftZone.quadrant = NPCZone.topRightQuad
    bottomLeftZone.startPoint = CGPoint(x: 164.0, y: 86.0)

    let bottomRightZone = NPCZoneSprite(color: SKColor.clear, size: CGSize(width: 280, height: 200))
    bottomRightZone.anchorPoint = CGPoint()
    bottomRightZone.position = CGPoint(x: 769, y: 0)
    bottomRightZone.zPosition = 100
    bottomRightZone.alpha = 0.25
    bottomRightZone.quadrant = NPCZone.topRightQuad
    bottomRightZone.startPoint = CGPoint(x: 922.0, y: 107.5)

    children.append(topStairsZone)
    zones.append(topStairsZone)

    children.append(middleStairsZone)
    zones.append(middleStairsZone)

    children.append(bottomLeftZone)
    zones.append(bottomLeftZone)

    children.append(bottomRightZone)
    zones.append(bottomRightZone)

    //Top Stairs Zone Paths
    var path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 400.5, y: 550.5))
    path.addLine(to: CGPoint(x: 348.5, y: 520.0))
    path.addLine(to: CGPoint(x: 240.5, y: 439.5))
    path.addLine(to: CGPoint(x: 215.0, y: 304.0))
    path.addLine(to: bottomLeftZone.startPoint)

    topStairsZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 400.5, y: 550.5))
    path.addLine(to: CGPoint(x: 348.5, y: 520.0))
    path.addLine(to: CGPoint(x: 240.5, y: 439.5))
    path.addLine(to: CGPoint(x: 215.0, y: 304.0))
    path.addLine(to: bottomRightZone.startPoint)

    topStairsZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 400.5, y: 550.5))
    path.addQuadCurve(to: CGPoint(x: 322.0, y: 141.999984741211), controlPoint: CGPoint(x: 134.5, y: 401.5))
    path.addQuadCurve(to: middleStairsZone.startPoint, controlPoint: CGPoint(x: 707.0, y: 35.0))

    topStairsZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 400.5, y: 550.5))
    path.addQuadCurve(to: CGPoint(x: 322.0, y: 141.999984741211), controlPoint: CGPoint(x: 134.5, y: 401.5))
    path.addLine(to: CGPoint(x: 681.0, y: 121.0))
    path.addLine(to: CGPoint(x: 726.0, y: 156.0))
    path.addLine(to: bottomRightZone.startPoint)

    topStairsZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 400.5, y: 550.5))
    path.addQuadCurve(to: CGPoint(x: 322.0, y: 141.999984741211), controlPoint: CGPoint(x: 134.5, y: 401.5))
    path.addLine(to: CGPoint(x: 681.0, y: 121.0))
    path.addLine(to: bottomRightZone.startPoint)

    topStairsZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 400.5, y: 550.5))
    path.addQuadCurve(to: CGPoint(x: 322.0, y: 141.999984741211), controlPoint: CGPoint(x: 134.5, y: 401.5))
    path.addLine(to: CGPoint(x: 508.5, y: 194.45))
    path.addLine(to: CGPoint(x: 748.5, y: 165.5))
    path.addQuadCurve(to: topStairsZone.startPoint, controlPoint: CGPoint(x: 890.0, y: 520.0))

    topStairsZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 560.5, y: 560.0))
    path.addQuadCurve(to: CGPoint(x: 720.0, y: 147.0), controlPoint: CGPoint(x: 923.0, y: 454.5))
    path.addQuadCurve(to: CGPoint(x: 343.0, y: 153.0), controlPoint: CGPoint(x: 511.5, y: 554.5))
    path.addLine(to: bottomLeftZone.startPoint)

    topStairsZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 560.5, y: 560.0))
    path.addQuadCurve(to: CGPoint(x: 720.0, y: 147.0), controlPoint: CGPoint(x: 923.0, y: 454.5))
    path.addLine(to: middleStairsZone.startPoint)

    topStairsZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 560.5, y: 560.0))
    path.addQuadCurve(to: CGPoint(x: 720.0, y: 147.0), controlPoint: CGPoint(x: 923.0, y: 454.5))
    path.addLine(to: bottomLeftZone.startPoint)

    topStairsZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 560.5, y: 560.0))
    path.addQuadCurve(to: CGPoint(x: 720.0, y: 147.0), controlPoint: CGPoint(x: 923.0, y: 454.5))
    path.addLine(to: bottomRightZone.startPoint)

    topStairsZone.roomPaths.append(path.cgPath)

    // Bottom Left Zone Paths
    path = UIBezierPath()
    path.move(to: bottomLeftZone.startPoint)
    path.addLine(to: CGPoint(x: 215.0, y: 304.0))
    path.addLine(to: CGPoint(x: 240.5, y: 439.5))
    path.addLine(to: CGPoint(x: 348.5, y: 520.0))
    path.addLine(to: CGPoint(x: 400.5, y: 550.5))
    path.addLine(to: topStairsZone.startPoint)

    bottomLeftZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: bottomLeftZone.startPoint)
    path.addQuadCurve(to: middleStairsZone.startPoint, controlPoint: CGPoint(x: 526.0, y: 47.5))

    bottomLeftZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: bottomLeftZone.startPoint)
    path.addQuadCurve(to: CGPoint(x: 256.5, y: 228.5), controlPoint: CGPoint(x: 115.0, y: 388.5))
    path.addQuadCurve(to: bottomRightZone.startPoint, controlPoint: CGPoint(x: 266.5, y: 13.5))

    bottomLeftZone.roomPaths.append(path.cgPath)

    // Middle Zone Paths
    path = UIBezierPath()
    path.move(to: middleStairsZone.startPoint)
    path.addQuadCurve(to: bottomLeftZone.startPoint, controlPoint: CGPoint(x: 526.0, y: 47.5))

    middleStairsZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: middleStairsZone.startPoint)
    path.addQuadCurve(to: bottomRightZone.startPoint, controlPoint: CGPoint(x: 328.5, y: 31.5))

    middleStairsZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 560.5, y: 560.0))
    path.addQuadCurve(to: CGPoint(x: 720.0, y: 147.0), controlPoint: CGPoint(x: 923.0, y: 454.5))
    path.addLine(to: middleStairsZone.startPoint)

    middleStairsZone.roomPaths.append(path.reversing().cgPath)

    path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 400.5, y: 550.5))
    path.addQuadCurve(to: CGPoint(x: 322.0, y: 141.999984741211), controlPoint: CGPoint(x: 134.5, y: 401.5))
    path.addQuadCurve(to: middleStairsZone.startPoint, controlPoint: CGPoint(x: 707.0, y: 35.0))

    middleStairsZone.roomPaths.append(path.reversing().cgPath)

    // Bottom Right Zone Paths
    path = UIBezierPath()
    path.move(to: middleStairsZone.startPoint)
    path.addQuadCurve(to: bottomRightZone.startPoint, controlPoint: CGPoint(x: 328.5, y: 31.5))

    bottomRightZone.roomPaths.append(path.reversing().cgPath)

    path = UIBezierPath()
    path.move(to: bottomRightZone.startPoint)
    path.addQuadCurve(to: bottomLeftZone.startPoint, controlPoint: CGPoint(x: 516.0, y: 292.0))

    bottomRightZone.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topStairsZone.startPoint)
    path.addLine(to: CGPoint(x: 560.5, y: 560.0))
    path.addQuadCurve(to: CGPoint(x: 720.0, y: 147.0), controlPoint: CGPoint(x: 923.0, y: 454.5))
    path.addLine(to: bottomRightZone.startPoint)

    bottomRightZone.roomPaths.append(path.reversing().cgPath)
  }

  open func getStartPosition(_ fromScene : RoomEnum) -> CGPoint {
    switch fromScene {
    case .libraryRoomType:
      return CGPoint(x: 80, y: 280)
    case .loungeRoomType:
      return CGPoint(x: 942, y: 280)
    default:
      return CGPoint(x: 512, y: 342)
    }
  }
}
