//
//  FoyerDoorMatSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/20/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class FoyerDoorMatSprite : GlowableSprite, InteractiveSprite {

  open func interact() {
    canInteract = false

    getRootSprite().run(SKAction.repeatForever(SKAction.sequence([
      SKAction.rotate(byAngle: 0.261799, duration: 0.5),
      SKAction.wait(forDuration: 0.05),
      SKAction.rotate(byAngle: -0.261799 * 2, duration: 0.5),
      SKAction.wait(forDuration: 0.05),
      SKAction.rotate(byAngle: 0.261799, duration: 0.5),
      SKAction.wait(forDuration: 0.05)
      ])))

    getRootSprite().run(SKAction.repeatForever(SKAction.sequence([
      SKAction.move(by: CGVector(dx: 90, dy:0), duration: 1.0),
      SKAction.wait(forDuration: 0.15),
      SKAction.move(by: CGVector(dx: -180, dy: 0), duration: 1.0),
      SKAction.wait(forDuration: 0.15),
      SKAction.move(by: CGVector(dx: 90, dy:0), duration: 1.0),
      SKAction.wait(forDuration: 0.15)
      ])))
  }
}
