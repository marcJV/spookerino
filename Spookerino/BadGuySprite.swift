//
//  BadGuySprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/18/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class BadGuySprite : ScaleableSprite {
  var canChangeRoom = false
  open var completedPath = true
  open var movementSpeed : CGFloat = 125
  open var shapeNode : SKShapeNode!
  open var defeated = false

  override init(texture: SKTexture?, color: UIColor, size: CGSize) {
    super.init(texture: texture, color: color, size: size)

    let collisionSize = CGRect(origin: CGPoint(x: size.width / -4, y: size.height / -4),
                               size: CGSize(width: size.width / 1.7, height: size.height / 1.5))

    shapeNode = SKShapeNode(rect: collisionSize)
    shapeNode.fillColor = SKColor.clear
    shapeNode.strokeColor = SKColor.clear

    addChild(shapeNode)
  }
  
  required public init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  open override func update(_ deltaTime: CFTimeInterval) {
    super.update(deltaTime)
  }

  open override func intersects(_ node: SKNode) -> Bool {
    return shapeNode.intersects(node)
  }

  open func checkFeetIntersection(_ node : SKNode) -> Bool {
    var feetRect = frame

    feetRect.size.height *= 0.2

    return feetRect.intersects(node.frame)
  }

  open func beginPath(_ path : CGPath) {
    if(completedPath) {
      completedPath = false

      let pathAction = SKAction.follow(path, asOffset: false, orientToPath: false, speed: movementSpeed)

      run(pathAction, completion: {() -> Void in
        let delayTime = DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
          self.completedPath = true
        }
      })
    }
  }
}
