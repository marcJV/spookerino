//
//  RoomSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/16/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class Room : SKSpriteNode, Touchable, Updateable {
  fileprivate var roomData : RoomData!

  open var roomChanger : RoomChanger?

  open var enemyDefeated : EnemyDefeatedNotifier?

  fileprivate var mainCharacter : GhostSprite = GhostSprite(imageNamed: "dude")

  fileprivate let screenHeight = UIScreen.main.bounds.height

  fileprivate var baddies = [BadGuySprite]()

  fileprivate var savedTouchPoint = CGPoint()

  open var damageTakenNotifier : DamageTakenNotifier!

  open var interactableSprites = [GlowableSprite]()

  open var finaleSprite : PossessableSprite?

  fileprivate var remainingInteractableSprites = true

  override init(texture: SKTexture?, color: UIColor, size: CGSize) {
    super.init(texture: texture, color: color, size: size)
  }

  required public init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setRoomData(_ roomData : RoomData) {
    anchorPoint = CGPoint()

    self.roomData = roomData
    texture = SKTexture(imageNamed: roomData.backgroundTextureName)
    size = UIScreen.main.bounds.size

    addChild(mainCharacter)

    let roomSprites = roomData.getChildren()

    for sprite in roomSprites {
      addChild(sprite)
    }

    for sprite in roomData.getGlowableSprites() {
      if sprite is InteractiveSprite {
        interactableSprites.append(sprite)
      } else if let possessSprite = sprite as? PossessableSprite {
        finaleSprite = possessSprite
      }
    }
  }

  func addBaddieInStartPosition(_ badGuy : BadGuySprite) {
    addChild(badGuy)
    badGuy.position = roomData.getBadGuyStartPosition()

    baddies.append(badGuy)
  }

  func didMoveToRoom(_ fromRoom : RoomEnum) {
    anchorPoint = CGPoint()

    //Do update animation?
    mainCharacter.didEnterRoom()
    mainCharacter.updatePosition(roomData.getStartPosition(fromRoom))

    for baddie in baddies {
      baddie.removeAllActions()
      baddie.completedPath = true
      baddie.position = roomData.getBadGuyStartPosition()
    }
  }

  open func beganTouchAtPoint(_ point: CGPoint, timeInterval : TimeInterval, longPressCompletion : @escaping LongPressCompletionNotifier) {

    //Check Doors
    for doorSprite in roomData.getDoors() {
      if doorSprite.containsPoint(point) && doorSprite.intersectsNode(mainCharacter) {
        mainCharacter.startTimer(doorSprite.timerDuration, longPressNotifier: {
          self.cancelledTouchAtPoint(point)
          self.roomChanger?(self.roomData.roomType, doorSprite.toRoom)
          
          longPressCompletion()
          }, timerColor : doorSprite.glowColor)
      }
    }

    //Check Local Interactive Sprites
    for glowSprite in interactableSprites {
      if glowSprite.containsPoint(point) && glowSprite.intersectsNode(mainCharacter) {
        mainCharacter.startTimer(glowSprite.timerDuration, longPressNotifier: {
          self.cancelledTouchAtPoint(point)

          if let interactiveSprite = glowSprite as? InteractiveSprite {
            interactiveSprite.interact()
          }

          var remaining = false
          for untouchedSprite in self.interactableSprites {
            if untouchedSprite.canInteract {
              remaining = true
              break
            }
          }

          if !remaining {
            self.remainingInteractableSprites = false
            self.finaleSprite?.possessable = true
          }

          //Notify Scene to stop tracking touch
          longPressCompletion()
          }, timerColor : glowSprite.glowColor)

        break
      }
    }

    //Check finale Sprite
    if !remainingInteractableSprites &&
      (finaleSprite as! GlowableSprite).containsPoint(point) &&
      (finaleSprite as! GlowableSprite).intersectsNode(mainCharacter) {

      if let finale = finaleSprite as? GlowableSprite {

      mainCharacter.startTimer(finale.timerDuration, longPressNotifier: {
        self.finaleSprite!.possess()

        self.enemyDefeated!()

        for enemy in self.baddies {
          enemy.defeated = true

          enemy.run(SKAction.fadeOut(withDuration: 0.5), completion : {
            enemy.removeFromParent()
          })
        }
        }, timerColor: finale.glowColor)
      }
    }
  }

  open func endedTouchAtPoint(_ point: CGPoint, timeInterval : TimeInterval) {
    if mainCharacter.showTimer {
      mainCharacter.cancelTimer()
    } else {
      mainCharacter.updateSpookyDestination(point)
    }
  }

  //TODO need callback for when longpress is completed

  open func clickedAtPoint(_ clickPoint : CGPoint) {
    mainCharacter.updateSpookyDestination(clickPoint)
  }

  open func cancelledTouchAtPoint(_ point: CGPoint) {
    mainCharacter.cancelTimer()
  }

  open func update(_ deltaTime: CFTimeInterval) {
    mainCharacter.update(deltaTime)

    for baddie in baddies {
      if !baddie.defeated {
        let screenPercentage = (screenHeight - baddie.position.y) / screenHeight
        let zPosition = screenPercentage * (100 - 1) + 1

        baddie.update(deltaTime)
        baddie.zPosition = zPosition
        baddie.overrideScale = roomData.getScaleForYPosition(baddie.position.y)

        //Check collision for ghost
        if mainCharacter.touchable && baddie.intersects(mainCharacter) {
          mainCharacter.setEthereal()
          
          damageTakenNotifier()
        }

        if baddie.completedPath {
          //Baddie isnt doing anything, give it a task
          for zone in roomData.getZones() {
            if baddie.checkFeetIntersection(zone) {
              if(zone.roomPaths.count > 0) {
                let path =  Int(arc4random_uniform(UInt32(zone.roomPaths.count)))

                baddie.beginPath(zone.roomPaths[path])
              }

              break
            }
          }
        }
      }
    }

    //Check for collision

    let screenPercentage = (screenHeight - mainCharacter.position.y) / screenHeight

    for glowable in roomData.getGlowableSprites() {
      let intersects = glowable.intersectsNode(mainCharacter)

      if intersects && !glowable.isGlowing {
        glowable.startGlowing()
      } else if !intersects && glowable.isGlowing {
        glowable.stopGlowing()
      }
    }
    
    mainCharacter.overrideScale = roomData.getScaleForYPosition(mainCharacter.position.y)
    
    //Update z indexes
    let zPosition = screenPercentage * (100 - 1) + 1
    
    mainCharacter.zPosition = zPosition
  }
}
