//
//  LibraryBookshelfSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/20/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class LibraryBookshelfSprite : GlowableSprite, InteractiveSprite {
  fileprivate var redBook : SKSpriteNode!
  fileprivate var greenBook : SKSpriteNode!
  fileprivate var blueBook : SKSpriteNode!

  override open func setupSprite(_ imageName: String) {
    super.setupSprite(imageName)

    let root = getRootSprite()

    var redBookPosition = position
    redBookPosition.y = -root.size.height / 2 + 14

    redBook = SKSpriteNode(imageNamed: "library-book-red")
    redBook.position = redBookPosition
    redBook.setScale(1.5)
    redBook.zPosition = zPosition + 1
    root.addChild(redBook)

    var greenBookPosition = position
    greenBookPosition.y = root.size.height / 2 - 34
    greenBookPosition.x = -root.size.width / 2 + 15

    greenBook = SKSpriteNode(imageNamed: "library-book-green")
    greenBook.position = greenBookPosition
    greenBook.zPosition = zPosition + 1
    root.addChild(greenBook)

    var blueBookPosition = position
    blueBookPosition.y = -24
    blueBookPosition.x = root.size.width / 2 - 15

    blueBook = SKSpriteNode(imageNamed: "library-book-blue")
    blueBook.position = blueBookPosition
    blueBook.zPosition = zPosition + 1

    root.addChild(blueBook)

    let spiderweb = SKSpriteNode(imageNamed: "library-bookcase-spiderweb")
    spiderweb.position = CGPoint(x: root.size.width / 2 - 25, y: root.size.height / 2 - 30)
    spiderweb.zPosition = zPosition + 2
    root.addChild(spiderweb)
  }

  open func flipHorizontal() {
    getRootSprite().xScale *= -1
  }

  open func interact() {
    canInteract = false

    let redbookUpAction = SKAction.move(by: CGVector(dx: 0, dy: 20), duration: 0.7)
    let redbookDownAction = SKAction.move(by: CGVector(dx: 0, dy: -20), duration: 0.7)

    redBook.run(SKAction.repeatForever(SKAction.sequence([redbookUpAction, redbookDownAction])))

    let leftAction = SKAction.move(by: CGVector(dx: -59, dy: 0), duration: 0.5)
    let rightAction = SKAction.move(by: CGVector(dx: 59, dy: 0), duration: 0.6)

    greenBook.run(SKAction.repeatForever(SKAction.sequence([rightAction, leftAction])))
    blueBook.run(SKAction.repeatForever(SKAction.sequence([leftAction, rightAction])))
  }
}
