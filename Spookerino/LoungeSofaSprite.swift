//
//  LoungeSofaSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/20/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class LoungeSofaSprite : GlowableSprite, InteractiveSprite {
  fileprivate var foregroundSofaSprite : SKSpriteNode!
  fileprivate var leftThrusterSprite : SKSpriteNode!
  fileprivate var rightThrusterSprite : SKSpriteNode!
  fileprivate var fireSprite : SKSpriteNode!

  fileprivate var fireSpotLeftSprite : SKSpriteNode!
  fileprivate var fireSpotRightSprite : SKSpriteNode!
  fileprivate var pressedSpotSprite : SKSpriteNode!

  override open func setupSprite(_ imageName: String) {
    super.setupSprite(imageName)

    let root = getRootSprite()

    leftThrusterSprite = SKSpriteNode(imageNamed: "lounge-jet-base")
    leftThrusterSprite.position = CGPoint(x: -90, y: -15)
    leftThrusterSprite.zPosition = zPosition + 2
    root.addChild(leftThrusterSprite)

    rightThrusterSprite = SKSpriteNode(imageNamed: "lounge-jet-base")
    rightThrusterSprite.position = CGPoint(x: 20, y: 80)
    rightThrusterSprite.zPosition = zPosition + 2
    root.addChild(rightThrusterSprite)

    fireSprite = SKSpriteNode(imageNamed: "lounge-jet-fire")
    fireSprite.position = CGPoint(x: -150, y: -89)
    fireSprite.zPosition = zPosition + 1
    fireSprite.alpha = 0
    root.addChild(fireSprite)

    fireSpotLeftSprite = SKSpriteNode(imageNamed: "lounge-fire-spot")
    fireSpotLeftSprite.position = CGPoint(x: -150, y: -135)
    fireSpotLeftSprite.zPosition = zPosition
    fireSpotLeftSprite.alpha = 0
    root.addChild(fireSpotLeftSprite)

    fireSpotRightSprite = SKSpriteNode(imageNamed: "lounge-fire-spot")
    fireSpotRightSprite.position = CGPoint(x: -20, y: -30)
    fireSpotRightSprite.zPosition = zPosition
    fireSpotRightSprite.alpha = 0
    root.addChild(fireSpotRightSprite)

    pressedSpotSprite = SKSpriteNode(imageNamed: "lounge-couch-spot")
    pressedSpotSprite.position = CGPoint(x: 27, y: -60)
    pressedSpotSprite.zPosition = zPosition
    pressedSpotSprite.alpha = 0.35
    root.addChild(pressedSpotSprite)

    foregroundSofaSprite = SKSpriteNode(imageNamed: imageName)
    foregroundSofaSprite.position = position
    foregroundSofaSprite.zPosition = zPosition + 3
    root.addChild(foregroundSofaSprite)

    root.texture = nil
    root.color = UIColor.clear
  }

  open func enginesOff() {
    fireSprite.removeAllActions()

    fireSprite.alpha = 0
    let fallAction = SKAction.move(by: CGVector(dx: 0, dy: -650), duration: 0.5)

    self.foregroundSofaSprite.run(fallAction)
    self.fireSprite.run(fallAction)
    self.rightThrusterSprite.run(fallAction)
    self.leftThrusterSprite.run(fallAction)

    let rotateSequence = SKAction.sequence([
      SKAction.wait(forDuration: 0.1),
      SKAction.rotate(byAngle: 1.5708, duration: 0.3)
      ])

    self.foregroundSofaSprite.run(rotateSequence)
    self.fireSprite.run(rotateSequence)
    self.rightThrusterSprite.run(rotateSequence)
    self.leftThrusterSprite.run(rotateSequence)
  }

  open func interact() {
    canInteract = false

    let leftJetEngineAction = SKAction.move(by: CGVector(dx: -60, dy: 0), duration: 0.35)
    let rightJetEngineAction = SKAction.move(by: CGVector(dx: -20, dy: 0), duration: 0.35)

    let showFireCompletion = {
      let scaleChange1 = SKAction.scaleX(to: -1, duration: 0)
      let scaleChange2 = SKAction.scaleX(to: 1, duration: 0)
      let waitAction = SKAction.wait(forDuration: 0.15)

      self.fireSprite.run(SKAction.repeatForever(SKAction.sequence([
        scaleChange1,
        waitAction,
        scaleChange2,
        waitAction
        ])), withKey: "firekey")

      let takeOffDelay : TimeInterval = 0.5
      let fadeInFireSpot = SKAction.fadeIn(withDuration: takeOffDelay)
      self.fireSpotRightSprite.run(fadeInFireSpot)
      self.fireSpotLeftSprite.run(fadeInFireSpot)

      let delayAction = SKAction.wait(forDuration: takeOffDelay)
      let launchAction = SKAction.move(by: CGVector(dx: 0, dy: 650), duration: 0.1)

      let takeOffAction = SKAction.sequence([delayAction, launchAction])
      self.foregroundSofaSprite.run(takeOffAction)
      self.fireSprite.run(takeOffAction)
      self.rightThrusterSprite.run(takeOffAction)
      self.leftThrusterSprite.run(takeOffAction)
    }

    let engineOutCompletion = {
      self.fireSprite.run(SKAction.fadeIn(withDuration: 0.2), completion: showFireCompletion)
    }

    leftThrusterSprite.run(leftJetEngineAction)
    rightThrusterSprite.run(rightJetEngineAction, completion: engineOutCompletion)



//    jeepSprite.runAction(SKAction.repeatActionForever(action))

  }
}
