//
//  GameScene.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/16/16.
//  Copyright (c) 2016 DrHaus. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
  var lastTime :CFTimeInterval = 0

  var savedTouch : UITouch? = nil
  var touchTime : TimeInterval = 0

  var roomManager = RoomManager()
  var hud = HudSprite()

  override func didMove(to view: SKView) {
    roomManager.setupRooms({
      self.hud.removeHeart()
      //Check for gameover

      if self.hud.getLifeRemaining() < 1 {
        self.showGameOverScene()
      }
    })

    roomManager.yellowDefeated = ({
      self.hud.defeatedYellowDude()

      if self.hud.getGhostHuntersRemaining() <= 0 {
        self.showWinScene()
      }
    })

    roomManager.greenDefeated = ({
      self.hud.defeatedGreenDude()

      if self.hud.getGhostHuntersRemaining() <= 0 {
        self.showWinScene()
      }
    })

    roomManager.purpleDefeated = ({
      self.hud.defeatedPurpleDude()

      if self.hud.getGhostHuntersRemaining() <= 0 {
        self.showWinScene()
      }
    })

    hud.setupHud()

    addChild(roomManager)
    addChild(hud)
  }

  fileprivate func showGameOverScene() {
    print("you died... again!")

    let transition = SKTransition.reveal(with: .down, duration: 1.0)

    let nextScene = GameOverScene(size: scene!.size)
    nextScene.scaleMode = .aspectFill

    scene?.view?.presentScene(nextScene, transition: transition)
  }

  fileprivate func showWinScene() {
    print("You win!")

    let transition = SKTransition.reveal(with: .down, duration: 1.0)

    let nextScene = YouWinScene(size: scene!.size)
    nextScene.scaleMode = .aspectFill

    scene?.view?.presentScene(nextScene, transition: transition)
  }

  let minLongTouchTime: TimeInterval = 0.5

  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    if let touch = touches.first {
      if savedTouch == nil {
        savedTouch = touch
        touchTime = touch.timestamp

        let delayTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
          if self.savedTouch != nil {
            self.roomManager.beganTouchAtPoint(touch.location(in: self),
                                               timeInterval: touch.timestamp,
                                               longPressCompletion: {
                                                self.savedTouch = nil
            })
          } else {
            self.roomManager.clickedAtPoint(touch.location(in: self))
          }
        }
      }
    }
  }

  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    if let sTouch = savedTouch, let touch = touches.first {
      let location = touch.location(in: self)
      let savedLocation = sTouch.location(in: self)

      if abs(savedLocation.x - location.x) < 10 && abs(savedLocation.y - location.y) < 10 {
        roomManager.endedTouchAtPoint(savedLocation, timeInterval: touch.timestamp)
      } else {
        roomManager.cancelledTouchAtPoint(savedLocation)
      }
    }

    savedTouch = nil
  }

  override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    if let sTouch = savedTouch{
      roomManager.cancelledTouchAtPoint(sTouch.location(in: self))
    }

    savedTouch = nil
  }

  override func update(_ currentTime: TimeInterval) {
    var deltaTime = currentTime - lastTime

    //First time the delta time is wacky
    if(deltaTime > 1) {
      deltaTime = 0
    }

    roomManager.update(deltaTime)
    hud.update(deltaTime)

    //TODO : always bring main character to front (or behind certain things)
    lastTime = currentTime
  }
}
