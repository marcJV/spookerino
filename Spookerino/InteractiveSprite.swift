//
//  Interactable.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/21/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation

public protocol InteractiveSprite {
  func interact()
}