//
//  LibraryLeftPictureSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/20/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class LibraryLeftPictureSprite : GlowableSprite, InteractiveSprite {
  fileprivate var badBackground : SKSpriteNode!
  fileprivate var lightningLeft : SKSpriteNode!
  fileprivate var lightningRight : SKSpriteNode!

  override open func setupSprite(_ imageName: String) {
    super.setupSprite(imageName)

    let root = getRootSprite()
    root.zPosition = 2

    let goodBackgroundSprite = SKSpriteNode(imageNamed: "library-picture-left-good")
    goodBackgroundSprite.zPosition = 3

    root.addChild(goodBackgroundSprite)

    badBackground = SKSpriteNode(imageNamed: "library-picture-left-bad")
    badBackground.alpha = 0
    badBackground.zPosition = 4

    root.addChild(badBackground)

    lightningLeft = SKSpriteNode(imageNamed: "lightning-left")
    lightningLeft.alpha = 0
    lightningLeft.zPosition = 5
    lightningLeft.position = CGPoint(x: root.size.width / -2 + 70, y: 40)

    root.addChild(lightningLeft)

    lightningRight = SKSpriteNode(imageNamed: "lightning-right")
    lightningRight.alpha = 0
    lightningRight.zPosition = 5
    lightningRight.position = CGPoint(x: root.size.width / 2 - 40, y: root.size.height / 2 - 70)

    root.addChild(lightningRight)

    let frameSprite = SKSpriteNode(imageNamed: imageName)
    frameSprite.zPosition = 5
    root.addChild(frameSprite)
  }

  open func flipHorizontal() {
    getRootSprite().xScale *= -1
  }

  open func interact() {
    canInteract = false

    let lightOnAction = SKAction.fadeAlpha(to: 1.0, duration: 0.01)
    let lightOffAction = SKAction.fadeAlpha(to: 0.0, duration: 0.01)

    badBackground.run(SKAction.fadeIn(withDuration: 0.2))

    lightningRight.run(SKAction.repeatForever(SKAction.sequence([
      lightOnAction,
      lightOffAction,
      lightOnAction,
      lightOffAction,
      SKAction.wait(forDuration: 0.5)
      ])))

    lightningLeft.run(SKAction.repeatForever(SKAction.sequence([
      SKAction.wait(forDuration: 0.25),
      lightOnAction,
      lightOffAction,
      lightOnAction,
      lightOffAction,
      SKAction.wait(forDuration: 0.25)
      ])))
  }
}
