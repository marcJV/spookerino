//
//  LibrarySofaPossessableSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/21/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class LibrarySofaPossessableSprite : GlowableSprite, PossessableSprite {
  open var possessable: Bool = false

  fileprivate var evilCouchSprite : SKSpriteNode!

  override open func setupSprite(_ imageName: String) {
    glowColor = UIColor.blue

    super.setupSprite(imageName)

    let root = getRootSprite()


    evilCouchSprite = SKSpriteNode(imageNamed: "library-couch-evil")
    evilCouchSprite.alpha = 0
    evilCouchSprite.zPosition = zPosition + 1
    root.addChild(evilCouchSprite)
  }

  open func possess() {
    possessable = false
    canInteract = false

    evilCouchSprite.run(SKAction.repeatForever(SKAction.sequence([
      SKAction.fadeIn(withDuration: 0.01),
      SKAction.wait(forDuration: 0.3),
      SKAction.fadeOut(withDuration: 0.01),
      SKAction.wait(forDuration: 0.03),
      ])))

    let bezierPath = UIBezierPath()
    bezierPath.move(to: CGPoint(x: 180, y: 148))
    bezierPath.addQuadCurve(to: CGPoint(x: 230, y: 148), controlPoint: CGPoint(x: 205, y: 200))

    let jumpAction = SKAction.follow(bezierPath.cgPath, asOffset: false, orientToPath:  false, duration: 0.5)
    let jumpAction2 = SKAction.follow(bezierPath.reversing().cgPath, asOffset: false, orientToPath:  false, duration: 0.5)

    let jumpSequence = SKAction.sequence([jumpAction, jumpAction2])

    getRootSprite().run(SKAction.repeatForever(jumpSequence))
  }
}
