//
//  ScaleableSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/18/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class ScaleableSprite : SKSpriteNode, Updateable {
  open var facingRight = true

  open var overrideScale : CGFloat = 1.0

  open func update(_ deltaTime: CFTimeInterval) {
    if(facingRight) {
      xScale = overrideScale
    } else {
      xScale = overrideScale * -1
    }

    yScale = overrideScale
  }
}
