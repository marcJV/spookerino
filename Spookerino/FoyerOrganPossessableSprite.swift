//
//  OrganPossessableSprite.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/21/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class FoyerOrganPossessableSprite : GlowableSprite, PossessableSprite {
  open var possessable: Bool = false

  fileprivate var fire1Sprite : SKSpriteNode!
  fileprivate var fire2Sprite : SKSpriteNode!
  fileprivate var fire3Sprite : SKSpriteNode!
  fileprivate var fire4Sprite : SKSpriteNode!
  fileprivate var fire5Sprite : SKSpriteNode!
  fileprivate var fire6Sprite : SKSpriteNode!
  fileprivate var fire7Sprite : SKSpriteNode!
  fileprivate var fire8Sprite : SKSpriteNode!
  fileprivate var erieMusicSprite : SKSpriteNode!

  override open func setupSprite(_ imageName: String) {
    glowColor = UIColor.blue

    super.setupSprite(imageName)

    let root = getRootSprite()

    let color = SKColor.orange
    let colorBlend : CGFloat = 0.65

    fire1Sprite = SKSpriteNode(imageNamed: "library-fire-small")
    fire1Sprite.position = CGPoint(x: -root.size.height / 2 + 1, y: root.size.height / 2 + 10)
    fire1Sprite.setScale(0.35)
    fire1Sprite.colorBlendFactor = colorBlend
    fire1Sprite.color = color
    fire1Sprite.alpha = 0
    fire1Sprite.zPosition = zPosition + 1
    root.addChild(fire1Sprite)

    fire2Sprite = SKSpriteNode(imageNamed: "library-fire-small")
    fire2Sprite.position = CGPoint(x: -root.size.height / 2 + 13, y: root.size.height / 2 - 6)
    fire2Sprite.setScale(0.3)
    fire2Sprite.colorBlendFactor = colorBlend
    fire2Sprite.color = color
    fire2Sprite.alpha = 0
    fire2Sprite.zPosition = zPosition + 1
    root.addChild(fire2Sprite)

    fire3Sprite = SKSpriteNode(imageNamed: "library-fire-small")
    fire3Sprite.position = CGPoint(x: -root.size.height / 2 + 25, y: root.size.height / 2 - 13)
    fire3Sprite.setScale(0.2)
    fire3Sprite.colorBlendFactor = colorBlend
    fire3Sprite.color = color
    fire3Sprite.alpha = 0
    fire3Sprite.zPosition = zPosition + 1
    root.addChild(fire3Sprite)

    fire4Sprite = SKSpriteNode(imageNamed: "library-fire-small")
    fire4Sprite.position = CGPoint(x: -root.size.height / 2 + 36.75, y: root.size.height / 2 - 22.5)
    fire4Sprite.setScale(0.15)
    fire4Sprite.colorBlendFactor = colorBlend
    fire4Sprite.color = color
    fire4Sprite.alpha = 0
    fire4Sprite.zPosition = zPosition + 1
    root.addChild(fire4Sprite)

    fire5Sprite = SKSpriteNode(imageNamed: "library-fire-small")
    fire5Sprite.position = CGPoint(x: root.size.height / 2 - 31.75, y: root.size.height / 2 - 22.5)
    fire5Sprite.setScale(0.15)
    fire5Sprite.colorBlendFactor = colorBlend
    fire5Sprite.color = color
    fire5Sprite.alpha = 0
    fire5Sprite.zPosition = zPosition + 1
    root.addChild(fire5Sprite)

    fire6Sprite = SKSpriteNode(imageNamed: "library-fire-small")
    fire6Sprite.position = CGPoint(x: root.size.height / 2 - 20, y: root.size.height / 2 - 13)
    fire6Sprite.setScale(0.2)
    fire6Sprite.colorBlendFactor = colorBlend
    fire6Sprite.color = color
    fire6Sprite.alpha = 0
    fire6Sprite.zPosition = zPosition + 1
    root.addChild(fire6Sprite)

    fire7Sprite = SKSpriteNode(imageNamed: "library-fire-small")
    fire7Sprite.position = CGPoint(x: root.size.height / 2 - 7.5, y: root.size.height / 2 - 6)
    fire7Sprite.setScale(0.3)
    fire7Sprite.colorBlendFactor = colorBlend
    fire7Sprite.color = color
    fire7Sprite.alpha = 0
    fire7Sprite.zPosition = zPosition + 1
    root.addChild(fire7Sprite)

    fire8Sprite = SKSpriteNode(imageNamed: "library-fire-small")
    fire8Sprite.position = CGPoint(x: root.size.height / 2 + 4, y: root.size.height / 2 + 10)
    fire8Sprite.setScale(0.35)
    fire8Sprite.colorBlendFactor = colorBlend
    fire8Sprite.color = color
    fire8Sprite.alpha = 0
    fire8Sprite.zPosition = zPosition + 1
    root.addChild(fire8Sprite)

    erieMusicSprite = SKSpriteNode(imageNamed: "foyer-erie-music")
    erieMusicSprite.position = CGPoint(x: 0, y: -50)
    erieMusicSprite.alpha = 0
    erieMusicSprite.zPosition = zPosition + 1
    root.addChild(erieMusicSprite)
  }

  open func possess() {
    possessable = false
    canInteract = false

    let fireWait = SKAction.wait(forDuration: 0.15)

    let fireAction1 = SKAction.repeatForever(SKAction.sequence([
      SKAction.scaleX(to: -0.35, duration: 0),
      fireWait,
      SKAction.scaleX(to: 0.35, duration: 0),
      fireWait
      ]))

    let fireAction2 = SKAction.repeatForever(SKAction.sequence([
      SKAction.scaleX(to: 0.3, duration: 0),
      fireWait,
      SKAction.scaleX(to: -0.3, duration: 0),
      fireWait
      ]))

    let fireAction3 = SKAction.repeatForever(SKAction.sequence([
      SKAction.scaleX(to: -0.2, duration: 0),
      fireWait,
      SKAction.scaleX(to: 0.2, duration: 0),
      fireWait
      ]))

    let fireAction4 = SKAction.repeatForever(SKAction.sequence([
      SKAction.scaleX(to: 0.15, duration: 0),
      fireWait,
      SKAction.scaleX(to: -0.15, duration: 0),
      fireWait
      ]))

    fire1Sprite.run(fireAction1)
    fire2Sprite.run(fireAction2)
    fire3Sprite.run(fireAction3)
    fire4Sprite.run(fireAction4)
    fire5Sprite.run(fireAction4)
    fire6Sprite.run(fireAction3)
    fire7Sprite.run(fireAction2)
    fire8Sprite.run(fireAction1)

    //Sequences

    let fireLightAction1 = SKAction.sequence([
      SKAction.fadeIn(withDuration: 0.25),
      SKAction.wait(forDuration: 1),
      ])

    let fireLightAction2 = SKAction.sequence([
      SKAction.wait(forDuration: 0.25),
      SKAction.fadeIn(withDuration: 0.25),
      SKAction.wait(forDuration: 1),
      ])

    let fireLightAction3 = SKAction.sequence([
      SKAction.wait(forDuration: 0.5),
      SKAction.fadeIn(withDuration: 0.25),
      SKAction.wait(forDuration: 0.75),
      ])

    let fireLightAction4 = SKAction.sequence([
      SKAction.wait(forDuration: 0.75),
      SKAction.fadeIn(withDuration: 0.25),
      SKAction.wait(forDuration: 0.5),
      ])

    fire1Sprite.run(fireLightAction1)
    fire2Sprite.run(fireLightAction2)
    fire3Sprite.run(fireLightAction3)
    fire4Sprite.run(fireLightAction4)
    fire5Sprite.run(fireLightAction4)
    fire6Sprite.run(fireLightAction3)
    fire7Sprite.run(fireLightAction2)
    fire8Sprite.run(fireLightAction1)

    erieMusicSprite.run(SKAction.repeatForever(SKAction.sequence([
      SKAction.fadeIn(withDuration: 0.25),
      SKAction.wait(forDuration: 0.5),
      SKAction.fadeOut(withDuration: 0.25),
      SKAction.wait(forDuration: 0.25),
      ])))
  }
}
