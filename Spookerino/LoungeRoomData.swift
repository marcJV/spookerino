//
//  LoungeRoomData.swift
//  Spookerino
//
//  Created by Marc Vandehey on 8/17/16.
//  Copyright © 2016 DrHaus. All rights reserved.
//

import Foundation
import SpriteKit

open class LoungeRoomData  : RoomData {
  open var backgroundTextureName = "lounge-base"

  open let screenHeight = UIScreen.main.bounds.height

  fileprivate var children : [SKNode] = [SKNode]()

  fileprivate var doors = [DoorSprite]()

  fileprivate var glowable = [GlowableSprite]()

  fileprivate var zones = [NPCZoneSprite]()

  open var roomType: RoomEnum = RoomEnum.loungeRoomType

  open func getScaleForYPosition(_ yPosition : CGFloat) -> CGFloat {
    let measuredScreenPercentage = yPosition / screenHeight

    var characterScale : CGFloat = 1.0

    if measuredScreenPercentage < 0.35 {
      let tempScreenHeight = screenHeight * 0.35
      let tempScreenPercentage = (tempScreenHeight - yPosition) / tempScreenHeight

      characterScale = tempScreenPercentage * (0.85 - 0.6) + 0.6
    } else {
      characterScale = 0.6
    }

    return characterScale
  }

  open func getChildren() -> [SKNode] {
    if(children.count == 0) {
      setupChildren()
    }

    return children
  }

  open func getDoors() -> [DoorSprite] {
    return doors
  }

  open func getGlowableSprites() -> [GlowableSprite] {
    return glowable
  }

  open func getZones() -> [NPCZoneSprite] {
    return zones
  }

  open func getBadGuyStartPosition() -> CGPoint {
    if(getZones().count > 0) {
        let zone =  Int(arc4random_uniform(UInt32(getZones().count)))
        return getZones()[zone].startPoint
    }else {
      print("What the fuck guys?")
    }

    return CGPoint()
  }

  fileprivate func setupChildren() {
    let leftDoor = DoorSprite()
    leftDoor.setupSprite("lounge-door-left")
    leftDoor.position = CGPoint(x: 60, y: 278)
    leftDoor.zPosition = 2
    leftDoor.toRoom = RoomEnum.foyerRoomType
    children.append(contentsOf: leftDoor.getChildren())
    doors.append(leftDoor)
    glowable.append(leftDoor)

    let couch = LoungeSofaSprite()
    couch.setupSprite("lounge-couch")
    couch.position = CGPoint(x: 300, y: 200)
    couch.zPosition = 55
    couch.timerDuration = 5
    children.append(contentsOf: couch.getChildren())
    glowable.append(couch)

    let painting = LoungePictureSprite()
    painting.setupSprite("lounge-picture")
    painting.position = CGPoint(x: 620, y: 550)
    painting.zPosition = 1
    painting.timerDuration = 1.5
    children.append(contentsOf: painting.getChildren())
    glowable.append(painting)

    let table = FloatyMcFloatyTable()
    table.setupSprite("lounge-table")
    table.position = CGPoint(x: 625, y: 175)
    table.zPosition = 75
    table.timerDuration = 3
    children.append(contentsOf: table.getChildren())
    glowable.append(table)

    //Disabled because running out of time :(
//    let topDoor = DoorSprite()
//    topDoor.setupSprite("lounge-door-top")
//    topDoor.position = CGPoint(x: 685, y: 430)
//    topDoor.zPosition = 1
//    children.appendContentsOf(topDoor.getChildren())
//    doors.append(topDoor)
//    glowable.append(topDoor)

    let fireplace = LoungeFireplaceSprite()
    fireplace.setupSprite("lounge-fireplace")
    fireplace.position = CGPoint(x: 910, y: 265)
    fireplace.zPosition = 1
    fireplace.timerDuration = 7
    glowable.append(fireplace)
    children.append(contentsOf: fireplace.getChildren())

    let knob = LoungeKnobPossessableSprite()
    knob.setupSprite("lounge-knob")
    knob.position = CGPoint(x: 865, y: 360)
    knob.zPosition = 2
    knob.sofaSprite = couch
    glowable.append(knob)
    children.append(contentsOf: knob.getChildren())

    let tv = LoungeTVSprite()
    tv.setupSprite("lounge-tv")
    tv.position = CGPoint(x: 915, y: 600)
    tv.zPosition = 2
    tv.timerDuration = 2
    glowable.append(tv)
    children.append(contentsOf: tv.getChildren())

    setupQuadrantsData()
  }

  fileprivate func setupQuadrantsData() {
    //Zone Sprite Setup
    let topRightQuadrant = NPCZoneSprite(color: SKColor.clear, size: CGSize(width: 460, height: 150))
    topRightQuadrant.anchorPoint = CGPoint()
    topRightQuadrant.position = CGPoint(x: 525.5, y: 155.0)
    topRightQuadrant.zPosition = 100
    topRightQuadrant.alpha = 0.25
    topRightQuadrant.quadrant = NPCZone.topRightQuad
    topRightQuadrant.startPoint = CGPoint(x: 699.5, y: 343.0)

    let topLeftQuadrant = NPCZoneSprite(color: SKColor.clear, size: CGSize(width: 450, height: 150))
    topLeftQuadrant.anchorPoint = CGPoint()
    topLeftQuadrant.position = CGPoint(x: 40, y: 155.0)
    topLeftQuadrant.zPosition = 100
    topLeftQuadrant.alpha = 0.25
    topLeftQuadrant.quadrant = NPCZone.topLeftQuad
    topLeftQuadrant.startPoint = CGPoint(x: 299.5, y: 343.0)

    let bottomRightQuadrant = NPCZoneSprite(color: SKColor.clear, size: CGSize(width: 460, height: 150))
    bottomRightQuadrant.anchorPoint = CGPoint()
    bottomRightQuadrant.position = CGPoint(x: 525.5, y: 0)
    bottomRightQuadrant.zPosition = 100
    bottomRightQuadrant.alpha = 0.25
    bottomRightQuadrant.startPoint = CGPoint(x: 929.5, y: 110.0)
    bottomRightQuadrant.quadrant = NPCZone.bottomRightQuad

    let bottomLeftQuadrant = NPCZoneSprite(color: SKColor.clear, size: CGSize(width: 450, height: 150))
    bottomLeftQuadrant.anchorPoint = CGPoint()
    bottomLeftQuadrant.position = CGPoint(x: 40, y: 0)
    bottomLeftQuadrant.zPosition = 100
    bottomLeftQuadrant.alpha = 0.25
    bottomLeftQuadrant.startPoint = CGPoint(x: 122, y:100)
    bottomLeftQuadrant.quadrant = NPCZone.bottomLeftQuad

    children.append(topRightQuadrant)
    zones.append(topRightQuadrant)
    children.append(topLeftQuadrant)
    zones.append(topLeftQuadrant)
    children.append(bottomRightQuadrant)
    zones.append(bottomRightQuadrant)
    children.append(bottomLeftQuadrant)
    zones.append(bottomLeftQuadrant)

    //Top Right Quad paths

    var path = UIBezierPath()
    path.move(to: topRightQuadrant.startPoint)
    path.addLine(to: topLeftQuadrant.startPoint)

    topRightQuadrant.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topRightQuadrant.startPoint)
    path.addLine(to: topLeftQuadrant.startPoint)

    topRightQuadrant.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topRightQuadrant.startPoint)
    path.addLine(to: CGPoint(x: 803.0, y:247.5))
    path.addLine(to: CGPoint(x: 699.5, y: 340.0))
    path.addLine(to: topLeftQuadrant.startPoint)

    topRightQuadrant.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topRightQuadrant.startPoint)
    path.addLine(to: CGPoint(x: 803.0, y:247.5))
    path.addLine(to: CGPoint(x: 929.5, y: 110.0))

    topRightQuadrant.roomPaths.append(path.cgPath)

    // Bottom Right Quad Paths

    path = UIBezierPath()
    path.move(to: bottomRightQuadrant.startPoint)
    path.addLine(to: CGPoint(x: 815.0, y: 107.0))
    path.addLine(to: CGPoint(x: 816.5, y: 221))
    path.addLine(to: topRightQuadrant.startPoint)

    bottomRightQuadrant.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: bottomRightQuadrant.startPoint)
    path.addLine(to: CGPoint(x: 815.0, y: 102.0))
    path.addLine(to: CGPoint(x: 459.0, y: 140))
    path.addLine(to: bottomLeftQuadrant.startPoint)
    
    bottomRightQuadrant.roomPaths.append(path.cgPath)

    //Top Left Quad paths

    path = UIBezierPath()
    path.move(to: topLeftQuadrant.startPoint)
    path.addLine(to: CGPoint(x: 507.5, y: 346.5))
    path.addLine(to: CGPoint(x: 423.5, y: 60))
    path.addLine(to: bottomLeftQuadrant.startPoint)

    topLeftQuadrant.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: topLeftQuadrant.startPoint)
    path.addLine(to: topRightQuadrant.startPoint)

    topLeftQuadrant.roomPaths.append(path.cgPath)

    //Bottom Left Quad paths

    path = UIBezierPath()
    path.move(to: bottomLeftQuadrant.startPoint)
    path.addLine(to: CGPoint(x: 370.5, y: 43.5))
    path.addLine(to: CGPoint(x: 453.0, y: 131.49))
    path.addLine(to: CGPoint(x: 488.0, y: 291.0))
    path.addLine(to: CGPoint(x: 528.0, y: 338.5))
    path.addLine(to: topRightQuadrant.startPoint)

    bottomLeftQuadrant.roomPaths.append(path.cgPath)

    path = UIBezierPath()
    path.move(to: bottomLeftQuadrant.startPoint)
    path.addLine(to: CGPoint(x: 462.5, y: 76.5))
    path.addLine(to: CGPoint(x: 437.0, y: 129.0))
    path.addLine(to: CGPoint(x: 486.0, y: 291.5))
    path.addLine(to: CGPoint(x: 631.0, y: 354.0))
    path.addLine(to: topRightQuadrant.startPoint)

    bottomLeftQuadrant.roomPaths.append(path.cgPath)
  }

  //Only one way in for now, ignore fromScene and set start point
  open func getStartPosition(_ fromScene : RoomEnum) -> CGPoint {
    return CGPoint(x: 80, y: 278)
  }
}
